// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef VENDORDOCUMENTSPRINTER_H
#define VENDORDOCUMENTSPRINTER_H

// Local includes
#include "VendorItemListModel.h"
#include "shared/Flohmarkt.h"

// Qt includes
#include <QDialog>

// Qt classes
class QJsonObject;

// Local classes
class TemporaryFile;

class VendorDocumentsPrinter : public QDialog
{
    Q_OBJECT

public:
    explicit VendorDocumentsPrinter(QWidget *parent, const Flohmarkt::VendorData &vendorData,
                                    const QVector<VendorItemListModel::ItemData> &itemData,
                                    int surcharge);

private: // Functions
    int price(int originalPrice) const;

private: // Variables
    TemporaryFile *m_file;
    const double m_surchargeFactor;

};

#endif // VENDORDOCUMENTSPRINTER_H
