// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "VendorDocumentsWidget.h"
#include "VendorItemList.h"
#include "VendorItemListModel.h"
#include "VendorItemListDelegate.h"
#include "VendorDocumentsPrinter.h"
#include "shared/UuidHelper.h"
#include "shared/IconButton.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPushButton>
#include <QMessageBox>
#include <QStandardPaths>
#include <QFileDialog>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonDocument>
#include <QDir>
#include <QSpinBox>

VendorDocumentsWidget::VendorDocumentsWidget(QWidget *parent) : QWidget(parent)
{
    auto *layout = new QVBoxLayout(this);

    // Toolbar

    auto *toolbarLayout = new QHBoxLayout;
    layout->addLayout(toolbarLayout);

    auto *openButton = new IconButton(tr("Öffnen"), QStringLiteral("document-open"), true);
    connect(openButton, &QPushButton::clicked, this, &VendorDocumentsWidget::openDocuments);
    toolbarLayout->addWidget(openButton);

    m_saveButton = new IconButton(tr("Speichern"), QStringLiteral("document-save"), true);
    connect(m_saveButton, &QPushButton::clicked, this, &VendorDocumentsWidget::saveDocuments);
    toolbarLayout->addWidget(m_saveButton);
    m_saveButton->setEnabled(false);

    auto *saveAsButton = new IconButton(tr("Speichern unter"), QStringLiteral("document-save-as"),
                                        true);
    connect(saveAsButton, &QPushButton::clicked, this, &VendorDocumentsWidget::saveDocumentsAs);
    toolbarLayout->addWidget(saveAsButton);

    auto *printButton = new IconButton(tr("Drucken"), QStringLiteral("document-print"), true);
    connect(printButton, &QPushButton::clicked, this, &VendorDocumentsWidget::printDocuments);
    toolbarLayout->addWidget(printButton);

    toolbarLayout->addStretch();

    // Vendor info

    auto *vendorBox = new QGroupBox(tr("Verkäufer-Info"));
    auto *vendorLayout = new QGridLayout(vendorBox);

    vendorLayout->addWidget(new QLabel(tr("Name:")), 0, 0);
    m_vendorName = new QLineEdit;
    m_vendorName->setMaxLength(80);
    connect(m_vendorName, &QLineEdit::editingFinished,
            this, &VendorDocumentsWidget::vendorNameEdited);
    vendorLayout->addWidget(m_vendorName, 0, 1);

    vendorLayout->addWidget(new QLabel(tr("Listennummer:")), 1, 0);
    m_listNumber = new QLineEdit;
    m_listNumber->setMaxLength(20);
    connect(m_listNumber, &QLineEdit::editingFinished,
            this, &VendorDocumentsWidget::listNumberEdited);
    vendorLayout->addWidget(m_listNumber, 1, 1);

    layout->addWidget(vendorBox);

    // Items info

    auto *itemsBox = new QGroupBox(tr("Artikelliste"));
    auto *itemsLayout = new QVBoxLayout(itemsBox);

    m_itemModel = new VendorItemListModel(this);
    connect(m_itemModel, &QAbstractItemModel::dataChanged,
            [this]
            {
                m_edited = true;
                m_saveButton->setEnabled(true);
            });

    auto *delegate = new VendorItemListDelegate(this);

    m_list = new VendorItemList;
    m_list->setModel(m_itemModel);
    m_list->setItemDelegate(delegate);
    m_list->setFocusPolicy(Qt::NoFocus);
    m_list->setColumnWidth(0, 500);
    m_list->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_list, &VendorItemList::customContextMenuRequested,
            this, &VendorDocumentsWidget::showContextMenu);
    connect(m_list, &VendorItemList::addItem, this, &VendorDocumentsWidget::addItem);

    itemsLayout->addWidget(m_list);
    layout->addWidget(itemsBox);

    // Control layout

    auto *controlLayout = new QHBoxLayout;
    itemsLayout->addLayout(controlLayout);

    auto *addItemButton = new QPushButton(tr("Artikel hinzufügen"));
    addItemButton->setIcon(QIcon(QStringLiteral(":/document-new.png")));
    addItemButton->setIconSize(QSize(24, 24));
    connect(addItemButton, &QPushButton::clicked, this, &VendorDocumentsWidget::addItem);
    controlLayout->addWidget(addItemButton);

    controlLayout->addStretch();

    controlLayout->addWidget(new QLabel(tr("Aufschlag für die gedruckte Liste:")));
    m_surcharge = new QSpinBox;
    m_surcharge->setMinimum(0);
    m_surcharge->setMaximum(999);
    m_surcharge->setSuffix(tr("\u202F%"));
    connect(m_surcharge, &QDoubleSpinBox::editingFinished,
            [this]
            {
                m_edited = true;
                m_saveButton->setEnabled(true);
            });
    controlLayout->addWidget(m_surcharge);

    // Context menu
    m_contextMenu = new QMenu(this);
    auto *deleteAction = m_contextMenu->addAction(tr("Artikel löschen"));
    connect(deleteAction, &QAction::triggered, this, &VendorDocumentsWidget::deleteItem);
}

void VendorDocumentsWidget::addItem()
{
    const auto row = m_itemModel->addItem();
    const auto index = m_itemModel->index(row, 0, QModelIndex());
    m_list->setCurrentIndex(index);
    m_list->edit(index);
}

void VendorDocumentsWidget::showContextMenu(QPoint point)
{
    const auto index = m_list->indexAt(point);
    if (! index.isValid()) {
        return;
    }

    m_selectedRow = index.row();
    point.setY(point.y() + m_list->horizontalHeader()->height());
    m_contextMenu->exec(m_list->mapToGlobal(point));
}

void VendorDocumentsWidget::deleteItem()
{
    const auto &item = m_itemModel->itemData(m_selectedRow);
    const auto description = item.description.isEmpty() ? tr("(unbenannt)") : item.description;
    if (QMessageBox::question(this,
            tr("Artikel löschen"),
            tr("Soll der Artikel „%1“ wirklich gelöscht werden?").arg(description))
        == QMessageBox::Yes) {

        m_itemModel->deleteItem(m_selectedRow);
    }
}

bool VendorDocumentsWidget::checkEdited()
{
    if (! m_edited) {
        return true;
    }

    const auto response = QMessageBox::question(this,
        tr("Verkäuferunterlagen erstellen"),
        tr("Es gibt ungespeicherte Änderungen. Sollen die Änderungen gespeichert werden?"),
        QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
        QMessageBox::Cancel);

    if (response == QMessageBox::Cancel) {
        return false;
    } else if (response == QMessageBox::Yes) {
        return saveDocuments();
    } else if (response == QMessageBox::No) {
        return true;
    }

    // We can't reach here
    return false;
}

bool VendorDocumentsWidget::openDocuments()
{
    if (! checkEdited()) {
        return false;
    }

    const auto fileName =  QFileDialog::getOpenFileName(this,
        tr("Verkäuferunterlagen öffnen"),
        QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first(),
        tr("JSON-Dokumente (*.json);;Alle Dateien (*)"));

    if (fileName.isEmpty()) {
        return false;
    }

    QFile file(fileName);

    if (! file.open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this,
            tr("Verkäuferunterlagen öffnen"),
            tr("Die Datei „%1“ konnte nicht zum Lesen geöffnet werden. Bitte die "
               "Zugriffsrechte überprüfen!").arg(QDir::toNativeSeparators(fileName)));
        return false;
    }

    const auto data = file.readAll();
    file.close();

    QJsonParseError parseError;
    const auto jsonDocument = QJsonDocument::fromJson(data, &parseError);
    if (parseError.error != QJsonParseError::NoError){
        QMessageBox::warning(this,
            tr("Verkäuferunterlagen öffnen"),
            tr("<p>Die Datei „%1“ konnte nicht geladen werden. Die Fehlermeldung war:</p>"
               "<p>Fehler bei Offset %2: %3</p>"
               "<p>Entweder ist Datei keine Flohmarkt-WWS-Verkäuferunterlagen-Liste, oder sie "
               "ist beschädigt.</p>").arg(
               QDir::toNativeSeparators(fileName),
               QString::number(parseError.offset),
               parseError.errorString()));
        return false;
    }

    const auto json = jsonDocument.object();

    switch (checkJsonMetadata(json)) {
    case VendorDocumentsWidget::MetaDataError:
        QMessageBox::warning(this,
            tr("Verkäuferunterlagen öffnen"),
            tr("<p>Die Datei „%1“ konnte nicht geladen werden. Die Metadaten sind fehlerhaft.</p>"
               "<p>Entweder ist Datei keine Flohmarkt-WWS-Verkäuferunterlagen-Liste, oder sie ist "
               "beschädigt.</p>").arg(QDir::toNativeSeparators(fileName)));
        return false;

    case VendorDocumentsWidget::ProtocolVersionError:
        QMessageBox::warning(this,
            tr("Verkäuferunterlagen öffnen"),
            tr("<p>Die Datei „%1“ konnte nicht geladen werden. Die Protokollversion ist unbekannt."
               "</p>"
               "<p>Entweder die Datei wurde mit einer neueren Version des Programms erstellt, oder "
               "sie ist keine Flohmarkt-WWS-Verkäuferunterlagen-Liste, oder sie ist beschädigt."
               "</p>").arg(QDir::toNativeSeparators(fileName)));
        return false;

    case VendorDocumentsWidget::MetaDataOkay:
        break;
    }

    const auto vendorInfo = json.value(Flohmarkt::Json::vendorInfo).toObject();
    bool vendorInfoOkay = true;
    if (   ! vendorInfo.contains(Flohmarkt::Json::vendorName)
        || ! vendorInfo.contains(Flohmarkt::Json::vendorListNumber)
        || ! vendorInfo.contains(Flohmarkt::Json::vendorId)
        || ! vendorInfo.contains(Flohmarkt::Json::vendorSurcharge)) {

        vendorInfoOkay = false;
    }

    if (vendorInfoOkay) {
        vendorInfoOkay = UuidHelper::checkUuid(vendorInfo.value(Flohmarkt::Json::vendorId));
    }

    if (! vendorInfoOkay) {
        QMessageBox::warning(this,
            tr("Verkäuferunterlagen öffnen"),
            tr("<p>Die Datei „%1“ konnte nicht geladen werden. Die Verkäuferinformationen sind "
               "unvollständig oder nicht lesbar.</p>"
               "<p>Entweder ist Datei keine Flohmarkt-WWS-Verkäuferunterlagen-Liste, oder sie ist "
               "beschädigt.</p>").arg(QDir::toNativeSeparators(fileName)));
        return false;
    }

    bool itemsListOkay = true;
    const auto itemsList = json.value(Flohmarkt::Json::itemsList).toArray();
    for (const auto &entry : itemsList) {
        const auto itemData = entry.toObject();
        if (   ! itemData.contains(Flohmarkt::Json::itemId)
            || ! itemData.contains(Flohmarkt::Json::itemDescription)
            || ! itemData.contains(Flohmarkt::Json::itemSize)
            || ! itemData.contains(Flohmarkt::Json::itemPrice)) {

            itemsListOkay = false;
            break;
        }

        if (! UuidHelper::checkUuid(itemData.value(Flohmarkt::Json::itemId))) {
            itemsListOkay = false;
            break;
        }
    }

    if (! itemsListOkay) {
        QMessageBox::warning(this,
            tr("Verkäuferunterlagen öffnen"),
            tr("<p>Die Datei „%1“ konnte nicht geladen werden. Die Artikelliste ist unvollständig "
               "oder nicht lesbar.</p>"
               "<p>Entweder ist Datei keine Flohmarkt-WWS-Verkäuferunterlagen-Liste, oder sie ist "
               "beschädigt.</p>").arg(QDir::toNativeSeparators(fileName)));
        return false;
    }

    // All checks passed, we can now actually load the thing :-)

    m_vendorData = Flohmarkt::VendorData {
        vendorInfo.value(Flohmarkt::Json::vendorId).toString(),
        vendorInfo.value(Flohmarkt::Json::vendorName).toString(),
        vendorInfo.value(Flohmarkt::Json::vendorListNumber).toString()
    };
    m_vendorName->setText(m_vendorData.name);
    m_listNumber->setText(m_vendorData.listNumber);
    m_surcharge->setValue(vendorInfo.value(Flohmarkt::Json::vendorSurcharge).toInt());

    m_itemModel->clear();
    for (const auto &entry : itemsList) {
        const auto item = entry.toObject();
        m_itemModel->addItem(VendorItemListModel::ItemData {
            item.value(Flohmarkt::Json::itemId).toString(),
            item.value(Flohmarkt::Json::itemDescription).toString(),
            item.value(Flohmarkt::Json::itemSize).toString(),
            item.value(Flohmarkt::Json::itemPrice).toInt()
        });
    }

    m_fileName = fileName;
    m_edited = false;
    m_saveButton->setEnabled(false);

    return true;
}

VendorDocumentsWidget::MetaDataCheckResult VendorDocumentsWidget::checkJsonMetadata(
    const QJsonObject &json) const
{
    if (! json.contains(Flohmarkt::Json::metadata)) {
        return VendorDocumentsWidget::MetaDataError;
    }

    const auto metadata = json.value(Flohmarkt::Json::metadata).toObject();
    if (   ! metadata.contains(Flohmarkt::Json::fileIdKey)
        || ! metadata.contains(Flohmarkt::Json::protocolVersionKey)) {

        return VendorDocumentsWidget::MetaDataError;
    }

    if (metadata.value(Flohmarkt::Json::fileIdKey).toString()
        != Flohmarkt::Json::fileId) {

        return VendorDocumentsWidget::MetaDataError;
    }

    if (metadata.value(Flohmarkt::Json::protocolVersionKey).toInt()
        != Flohmarkt::Json::protocolVersion) {

        return ProtocolVersionError;
    }

    return VendorDocumentsWidget::MetaDataOkay;
}

bool VendorDocumentsWidget::saveDocuments()
{
    if (m_fileName.isEmpty()) {
        return saveDocumentsAs();
    }

    return saveJsonData(m_fileName);
}

bool VendorDocumentsWidget::saveDocumentsAs()
{
    const auto fileName = QFileDialog::getSaveFileName(this,
        tr("Verkäuferunterlagen speichern"),
        QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first().append(
            m_fileName.isEmpty() ? QStringLiteral("/Verkäuferunterlagen.json") : m_fileName),
        tr("JSON-Dokumente (*.json);;Alle Dateien (*)"));

    if (! fileName.isEmpty()) {
        return saveJsonData(fileName);
    }

    return false;
}

void VendorDocumentsWidget::printDocuments()
{
    prepareVendorInfo();

    if (m_vendorData.name.isEmpty()) {
        QMessageBox::warning(this, tr("Verkäuferunterlagen drucken"),
            tr("Es wurde bisher kein Verkäufername angegeben. Bitte noch ergänzen!"));
        m_vendorName->setFocus();
        return;
    }

    const auto items = m_itemModel->items();
    if (items.isEmpty()) {
        QMessageBox::warning(this, tr("Verkäuferunterlagen drucken"),
            tr("Es wurden bisher keine Artikel erfasst!"));
        return;
    }

    for (const auto &item : items) {
        if (item.description.isEmpty()) {
            QMessageBox::warning(this, tr("Verkäuferunterlagen drucken"),
                tr("Es gibt Artikeleinträge ohne Beschreibung. Bitte entweder ergänzen, oder die "
                   "entsprechenden Artikel löschen!"));
            return;
        }
    }

    if (m_vendorData.listNumber.isEmpty()
        && QMessageBox::question(this, tr("Verkäuferunterlagen drucken"),
               tr("Es wurde keine Listennummer angegeben. Trotzdem fortfahren?"))
        == QMessageBox::No) {

        m_listNumber->setFocus();
        return;
    }

    auto *printer = new VendorDocumentsPrinter(this, m_vendorData, items, m_surcharge->value());
    printer->exec();
}

void VendorDocumentsWidget::prepareVendorInfo()
{
    m_vendorData.name = m_vendorName->text().simplified();
    m_vendorData.listNumber = m_listNumber->text().simplified();
    if (m_vendorData.id.isEmpty()) {
        m_vendorData.id = UuidHelper::generateUuid();
    }
}

bool VendorDocumentsWidget::saveJsonData(const QString &fileName)
{
    prepareVendorInfo();

    QJsonObject data;

    // File metadata
    data.insert(Flohmarkt::Json::metadata, QJsonValue(QJsonObject {
        { Flohmarkt::Json::fileIdKey,          Flohmarkt::Json::fileId },
        { Flohmarkt::Json::protocolVersionKey, Flohmarkt::Json::protocolVersion }
    }));

    // Vendor info
    data.insert(Flohmarkt::Json::vendorInfo, QJsonValue(QJsonObject {
        { Flohmarkt::Json::vendorName,       m_vendorData.name },
        { Flohmarkt::Json::vendorListNumber, m_vendorData.listNumber },
        { Flohmarkt::Json::vendorId,         m_vendorData.id },
        { Flohmarkt::Json::vendorSurcharge,  m_surcharge->value() }
    }));

    // Items
    QJsonArray items;
    for (const auto &item : m_itemModel->items()) {
        items.append(QJsonObject {
            { Flohmarkt::Json::itemId,          item.id },
            { Flohmarkt::Json::itemDescription, item.description },
            { Flohmarkt::Json::itemSize,        item.size },
            { Flohmarkt::Json::itemPrice,       item.price }
        });
    }
    data.insert(Flohmarkt::Json::itemsList, QJsonValue(items));

    // Save to file

    QFile file(fileName);

    if (! file.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(this,
            tr("Verkäuferunterlagen speichern"),
            tr("Die Datei „%1“ konnte nicht zum Schreiben geöffnet werden. Bitte die "
               "Zugriffsrechte überprüfen!").arg(QDir::toNativeSeparators(fileName)));
        return false;
    }

    if (! file.write(QJsonDocument(data).toJson(QJsonDocument::Indented))) {
        QMessageBox::warning(this,
            tr("Verkäuferunterlagen speichern"),
            tr("Die Daten konnten nicht in Datei „%1“ gespeichert werden. Bitte die "
               "Zugriffsrechte überprüfen!").arg(QDir::toNativeSeparators(fileName)));
        return false;
    }

    file.close();
    m_fileName = fileName;

    QMessageBox::information(this,
        tr("Verkäuferunterlagen speichern"),
        tr("Die Daten wurden in der Datei „%1“ gespeichert!").arg(
           QDir::toNativeSeparators(fileName)));

    m_edited = false;
    m_saveButton->setEnabled(false);
    return true;
}

void VendorDocumentsWidget::vendorNameEdited()
{
    const auto text = m_vendorName->text();
    if (m_vendorData.name != text) {
        m_vendorData.name = text;
        m_edited = true;
        m_saveButton->setEnabled(true);
    }
}

void VendorDocumentsWidget::listNumberEdited()
{
    const auto text = m_listNumber->text();
    if (m_vendorData.listNumber != text) {
        m_vendorData.listNumber = text;
        m_edited = true;
        m_saveButton->setEnabled(true);
    }
}

bool VendorDocumentsWidget::clear(bool checkIfEdited)
{
    if (checkIfEdited && ! checkEdited()) {
        return false;
    }

    m_vendorName->clear();
    m_listNumber->clear();
    m_surcharge->setValue(0.0);
    m_vendorData = Flohmarkt::VendorData { };
    m_itemModel->clear();

    m_saveButton->setEnabled(false);
    m_edited = false;

    return true;
}
