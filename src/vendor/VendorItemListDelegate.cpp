// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "VendorItemListDelegate.h"

// Qt includes
#include <QDebug>
#include <QLineEdit>
#include <QDoubleSpinBox>
#include <QLocale>
#include <QKeyEvent>

VendorItemListDelegate::VendorItemListDelegate(QWidget *parent) : QStyledItemDelegate(parent)
{
    QLocale locale;
    m_currencySymbol = locale.currencySymbol();
}

QWidget *VendorItemListDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &,
                                              const QModelIndex &index) const
{
    const auto column = index.column();
    if (column == 0) {
        // Description
        auto *editor = new QLineEdit(parent);
        editor->setMaxLength(80);
        return editor;

    } else if (column == 1) {
        // Size
        auto *editor = new QLineEdit(parent);
        editor->setMaxLength(20);
        return editor;

    } else if (column == 2) {
        // Price
        auto *editor = new QDoubleSpinBox(parent);
        editor->setMinimum(0.0);
        editor->setMaximum(9999.99);
        editor->setSingleStep(0.5);
        editor->setSuffix(tr("\u202F%1").arg(m_currencySymbol));
        return editor;
    }

    // We can't reach here
    return new QLineEdit(parent);
}
