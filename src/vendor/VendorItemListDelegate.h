// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef VENDORITEMLISTDELEGATE_H
#define VENDORITEMLISTDELEGATE_H

// Qt includes
#include <QStyledItemDelegate>

class VendorItemListDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit VendorItemListDelegate(QWidget *parent = nullptr);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
                          const QModelIndex &index) const override;

private: // Variables
    QString m_currencySymbol;

};

#endif // VENDORITEMLISTDELEGATE_H
