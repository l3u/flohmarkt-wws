// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "VendorItemList.h"
#include "VendorItemListModel.h"

// Qt includes
#include <QDebug>
#include <QMouseEvent>

VendorItemList::VendorItemList(QWidget *parent) : QTableView(parent)
{
}

void VendorItemList::closeEditor(QWidget *editor, QAbstractItemDelegate::EndEditHint hint)
{
    if (hint == QAbstractItemDelegate::EditNextItem) {
        const auto index = currentIndex();
        if (index.column() == 2 && index.row() == model()->rowCount() - 1) {
            static_cast<VendorItemListModel *>(model())->addItem();
        }
    }

    QTableView::closeEditor(editor, hint);
}

void VendorItemList::mouseDoubleClickEvent(QMouseEvent *event)
{
    const auto index = indexAt(event->pos());
    if (! index.isValid()) {
        Q_EMIT addItem();
    }
    QTableView::mouseDoubleClickEvent(event);
}
