// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef VENDORITEMLIST_H
#define VENDORITEMLIST_H

// Qt includes
#include <QTableView>

// Qt classes
class QMouseEvent;

class VendorItemList : public QTableView
{
    Q_OBJECT

public:
    explicit VendorItemList(QWidget *parent = nullptr);

Q_SIGNALS:
    void addItem();

protected Q_SLOTS:
    void closeEditor(QWidget *editor, QAbstractItemDelegate::EndEditHint hint) override;

protected: // Functions
    void mouseDoubleClickEvent(QMouseEvent *event) override;

};

#endif // VENDORITEMLIST_H
