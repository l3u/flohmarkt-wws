// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef VENDORDOCUMENTSWIDGET_H
#define VENDORDOCUMENTSWIDGET_H

// Local includes
#include "shared/Flohmarkt.h"

// Qt includes
#include <QWidget>
#include <QMenu>

// Local classes
class VendorItemList;
class VendorItemListModel;

// Qt classes
class QLineEdit;
class QPushButton;
class QSpinBox;

class VendorDocumentsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit VendorDocumentsWidget(QWidget *parent = nullptr);
    bool checkEdited();
    bool clear(bool checkIfEdited = true);

public Q_SLOTS:
    bool openDocuments();

private Q_SLOTS:
    void vendorNameEdited();
    void listNumberEdited();

    void addItem();

    bool saveDocuments();
    bool saveDocumentsAs();
    void printDocuments();

    void showContextMenu(QPoint point);
    void deleteItem();

private: // Enums
    enum MetaDataCheckResult {
        MetaDataOkay,
        MetaDataError,
        ProtocolVersionError
    };

private: // Functions
    void prepareVendorInfo();
    bool saveJsonData(const QString &fileName);
    MetaDataCheckResult checkJsonMetadata(const QJsonObject &json) const;

private: // Variables
    QPushButton *m_saveButton;

    QLineEdit *m_vendorName;
    QLineEdit *m_listNumber;
    QSpinBox *m_surcharge;

    QString m_fileName;
    Flohmarkt::VendorData m_vendorData;

    VendorItemListModel *m_itemModel;
    VendorItemList *m_list;
    int m_selectedRow = 0;
    QMenu *m_contextMenu;

    bool m_edited = false;

};

#endif // VENDORDOCUMENTSWIDGET_H
