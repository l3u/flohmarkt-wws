// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef VENDORITEMLISTMODEL_H
#define VENDORITEMLISTMODEL_H

// Qt includes
#include <QAbstractTableModel>
#include <QLocale>
#include <QVector>

class VendorItemListModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    struct ItemData
    {
        QString id;
        QString description;
        QString size;
        int price;
    };

    explicit VendorItemListModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &) const override;
    int columnCount(const QModelIndex &) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    int addItem();
    void addItem(const ItemData &data);
    void deleteItem(int row);
    const ItemData &itemData(int row) const;
    const QVector<ItemData> &items() const;
    void clear();

private: // Variables
    const QLocale m_locale;
    QVector<ItemData> m_items;

};

#endif // VENDORITEMLISTMODEL_H
