// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "VendorItemListModel.h"
#include "shared/UuidHelper.h"

// Qt includes
#include <QDebug>

VendorItemListModel::VendorItemListModel(QObject *parent) : QAbstractTableModel(parent)
{
}

int VendorItemListModel::rowCount(const QModelIndex &) const
{
    return m_items.count();
}

int VendorItemListModel::columnCount(const QModelIndex &) const
{
    return 3;
}

QVariant VendorItemListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    switch (orientation) {
    case Qt::Horizontal:
        if (section == 0) {
            return tr("Beschreibung");
        } else if (section == 1) {
            return tr("Größe");
        } else if (section == 2) {
            return tr("Preis");
        }
        break;
    case Qt::Vertical:
        return QVariant(section + 1);
    }

    // We can't reach here
    return QVariant();
}

QVariant VendorItemListModel::data(const QModelIndex &index, int role) const
{
    if (! index.isValid()) {
        return QVariant();
    }

    const auto column = index.column();

    if (role == Qt::TextAlignmentRole) {
        if (column == 2) {
            return int(Qt::AlignRight | Qt::AlignVCenter);
        } else {
            return QVariant();
        }
    }

    const auto row = index.row();
    if (row >= m_items.count() || (role != Qt::DisplayRole && role != Qt::EditRole)) {
        return QVariant();
    }

    if (column == 0) {
        return m_items.at(row).description;
    } else if (column == 1) {
        return m_items.at(row).size;
    } else if (column == 2) {
        if (role == Qt::DisplayRole) {
            return m_locale.toCurrencyString(m_items.at(row).price / 100.0);
        } else if (role == Qt::EditRole) {
            return m_items.at(row).price / 100.0;
        } else {
            return m_items.at(row).price;
        }
    }

    // We can't reach here
    return QVariant();
}

Qt::ItemFlags VendorItemListModel::flags(const QModelIndex &) const
{
    return Qt::ItemIsEnabled | Qt::ItemIsEditable;
}

bool VendorItemListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (! index.isValid()) {
        return false;
    }

    const auto row = index.row();
    if (row >= m_items.count() || role != Qt::EditRole) {
        return false;
    }

    const auto column = index.column();
    if (column == 0) {
        m_items[row].description = value.toString().simplified();
    } else if (column == 1) {
        m_items[row].size = value.toString().simplified();
    } else if (column == 2) {
        m_items[row].price = int(value.toDouble() * 100.0);
    }

    Q_EMIT dataChanged(index, index, { Qt::DisplayRole });
    return true;
}

int VendorItemListModel::addItem()
{
    const int row = m_items.count();
    beginInsertRows(QModelIndex(), row, row);
    m_items.append(ItemData { UuidHelper::generateUuid(),
                              QString(),
                              QString(),
                              0 });
    endInsertRows();
    const auto modelIndex = index(row, 0);
    Q_EMIT dataChanged(modelIndex, modelIndex, { Qt::DisplayRole });
    return row;
}

void VendorItemListModel::addItem(const VendorItemListModel::ItemData &data)
{
    const int row = m_items.count();
    beginInsertRows(QModelIndex(), row, row);
    m_items.append(data);
    endInsertRows();
    const auto modelIndex = index(row, 0);
    Q_EMIT dataChanged(modelIndex, modelIndex, { Qt::DisplayRole });
}

void VendorItemListModel::deleteItem(int row)
{
    beginRemoveRows(QModelIndex(), row, row);
    m_items.remove(row);
    endRemoveRows();
}

void VendorItemListModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, m_items.count() - 1);
    m_items.clear();
    endRemoveRows();
}

const VendorItemListModel::ItemData &VendorItemListModel::itemData(int row) const
{
    Q_ASSERT(row <= m_items.count());
    return m_items[row];
}

const QVector<VendorItemListModel::ItemData> &VendorItemListModel::items() const
{
    return m_items;
}
