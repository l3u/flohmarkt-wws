// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "VendorDocumentsPrinter.h"
#include "shared/HtmlGenerator.h"
#include "shared/QrCodeGenerator.h"
#include "shared/TemporaryFile.h"

// Qt includes
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QLabel>
#include <QDebug>
#include <QVector>
#include <QJsonObject>
#include <QDesktopServices>

// C++ includes
#include <cmath>

static constexpr auto s_black = qRgb(  0,   0,   0);
static constexpr auto s_white = qRgb(255, 255, 255);

static const QString s_style = QStringLiteral(R"END(
body {
    font-size: 12pt;
}

img.vendor_qr {
    width: 2.2cm;
    height: 2.2cm;
}

img.item_qr {
    width: 3cm;
    height: 3cm;
}

table {
    border-spacing: 0;
}

th, td {
    padding: 0.2em 0.8em 0.2em;
    text-align: left;
}

span.spacer {
    margin-right: 2em;
}

#vendor_table td {
    border: none;
}

#vendor_table p:first-child {
    font-size: 200%;
    margin-top: 0.4em;
    margin-bottom: -0.5em;
}

#vendor_table p:last-child {
    font-size: 120%;
}

#items_list tr td {
    border-top: 1px solid black;
}

#items_list tr td:nth-child(4) {
    text-align: right;
}

#items_labels tr th {
    border-bottom: 1px solid black;
}

#items_labels tr td {
    border-bottom: 1px solid black;
}

#items_labels tr td:nth-child(1) p:nth-child(2) {
    font-size: 140%;
}

#items_labels tr td:nth-child(2) {
    font-size: 200%;
    font-weight: bold;
    text-align: right;
}

@media print {
    @page {
        margin: 1.5cm 2cm;
    }

    #items_labels {
        page-break-before: always;
    }
}

)END");

VendorDocumentsPrinter::VendorDocumentsPrinter(QWidget *parent,
    const Flohmarkt::VendorData &vendorData,
    const QVector<VendorItemListModel::ItemData> &itemData,
    int surcharge)

    : QDialog(parent),
      m_surchargeFactor(surcharge == 0 ? 1.0 : (1.0 + (surcharge / 100.0)))
{
    setAttribute(Qt::WA_DeleteOnClose, true);

    m_file = new TemporaryFile(this);
    m_file->open();

    auto *layout = new QVBoxLayout(this);
    auto *label = new QLabel(tr(
        "<h2>Drucken der Verkaufsliste</h2>"
        "<p>Es wird nun eine HTML-Datei mit der Verkaufsliste erstellt und mit dem "
        "Standard-Webbrowser geöffnet. Damit kann die Liste dann gedruckt werden.</p>"
        "<p>Der Dateiname ist:<br>"
        "<kbd><a href=\"%1\">%2</a></kbd></p>"
        "<p>Die Datei wird automatisch gelöscht, sobald dieser Dialog geschlossen wird.</p>").arg(
        m_file->htmlUrl(), m_file->displayName()));
    label->setWordWrap(true);
    label->setTextInteractionFlags(Qt::TextBrowserInteraction);
    label->setOpenExternalLinks(true);
    layout->addWidget(label);

    auto *buttonBox = new QDialogButtonBox(QDialogButtonBox::Close);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    layout->addWidget(buttonBox);

    // Create the HTML file

    HtmlGenerator html(m_file);
    html.setTitle(tr("Verkaufsliste für %1").arg(vendorData.name).toHtmlEscaped());
    html.setStyle(s_style);
    html.startHtml();

    int number = 0;

    // Insert the vendor id card

    html << "<table id=\"vendor_table\">\n"
         << "<tr>\n"
         << "<td>\n"
         << "<img class=\"vendor_qr\" alt=\""
         << tr("Verkäufer-QR-Code für %1").arg(vendorData.name).toHtmlEscaped() << "\" "
         << "src=\""
         << QrCodeGenerator::base64Source(QrCodeGenerator::qrCode(
                QJsonObject { { Flohmarkt::Qr::vendorId,         vendorData.id },
                              { Flohmarkt::Qr::vendorListNumber, vendorData.listNumber },
                              { Flohmarkt::Qr::vendorName,
                                    QrCodeGenerator::escape(vendorData.name) } },
                Flohmarkt::Qr::VendorCode))
         << "\">\n"
         << "</td>\n"
         << "<td>\n"
         << "<p>" << vendorData.name.toHtmlEscaped() << "</p>\n"
         << "<p>";

    if (! vendorData.listNumber.isEmpty()) {
        html << tr("Listennummer %1").arg(vendorData.listNumber);
    } else {
        html << tr("Verkäufer-ID: %1").arg(vendorData.id);
    }

    html << "</p>\n"
         << "</td>\n"
         << "</tr>\n"
         << "</table>\n\n";

    // Insert the item list

    const QLocale locale;
    number = 0;

    html << "<table id=\"items_list\">\n"
         << "<thead>\n"
         << "<tr>"
         << "<th>" << tr("Nummer") << "</th>"
         << "<th>" << tr("Beschreibung") << "</th>"
         << "<th>" << tr("Größe") << "</th>"
         << "<th>" << tr("Preis") << "</th>"
         << "<th>" << tr("Erfasst") << "</th>"
         << "</tr>\n"
         << "</thead>\n"
         << "<tbody>\n";

    for (const auto & item : itemData) {
        html << "<tr>"
             << "<td>" << QString::number(++number) << "</td>"
             << "<td>" << item.description.toHtmlEscaped() << "</td>"
             << "<td>" << (item.size.isEmpty() ? tr("–") : item.size.toHtmlEscaped()) << "</td>"
             << "<td>" << locale.toCurrencyString(price(item.price) / 100.0) << "</td>"
             << "<td></td>\n"
             << "</tr>\n";
    }

    html << "</tbody>\n"
         << "</table>\n\n";

    // Insert the item labels

    html << "<table id=\"items_labels\">\n"
         << "<thead>\n"
         << "<tr><th colspan=\"3\"></th></tr>\n"
         << "</thead>\n"
         << "<tbody>\n";

    number = 0;
    for (const auto & item : itemData) {
        number++;
        html << "<tr>"
             << "<td>\n"
             << "<p>";

        if (! vendorData.listNumber.isEmpty()) {
            html << tr("Liste: <b>%1</b><span class=\"spacer\"></span>"
                       "Nummer: <b>%2</b>").arg(vendorData.listNumber, QString::number(number));
        } else {
            html << tr("Verkäufer: %1<br>Artikelnummer: %2").arg(vendorData.id,
                                                                 QString::number(number));
        }

        html << "</p>\n"
             << "<p>" << item.description.toHtmlEscaped() << "</p>\n";

        if (! item.size.isEmpty()) {
            html << "<p>" << tr("Größe: %1").arg(item.size.toHtmlEscaped()) << "</p>\n";
        }

        html << "</td>\n"
             << "<td>" << locale.toCurrencyString(price(item.price) / 100.0) << "</td>\n"
             << "<td>\n"
             << "<img class=\"item_qr\" "
             << "alt=\"" << tr("Artikel-QR-Code für Artikel Nr. %1").arg(number) << "\" "
             << "src=\""
             << QrCodeGenerator::base64Source(QrCodeGenerator::qrCode(
                    QJsonObject { { Flohmarkt::Qr::vendorId,        vendorData.id },
                                  { Flohmarkt::Qr::itemId,          item.id },
                                  { Flohmarkt::Qr::itemNumber,      number },
                                  { Flohmarkt::Qr::itemDescription,
                                        QrCodeGenerator::escape(item.description) },
                                  { Flohmarkt::Qr::itemSize,
                                        QrCodeGenerator::escape(item.size) },
                                  { Flohmarkt::Qr::itemPrice,       price(item.price) } },
                    Flohmarkt::Qr::ItemCode))
             << "\">\n"
             << "</td>\n"
             << "</tr>\n";
    }

    html << "</tbody>\n"
         << "</table>\n\n";

    html.finishHtml();
    m_file->close();

    QDesktopServices::openUrl(m_file->url());
}

int VendorDocumentsPrinter::price(int originalPrice) const
{
    return std::round(originalPrice * m_surchargeFactor);
}
