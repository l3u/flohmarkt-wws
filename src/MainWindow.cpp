// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "MainWindow.h"
#include "WelcomeWidget.h"
#include "vendor/VendorDocumentsWidget.h"
#include "organizer/FleaMarketWidget.h"
#include "AboutDialog.h"

// Qt includes
#include <QDebug>
#include <QStackedLayout>
#include <QCloseEvent>
#include <QMenuBar>
#include <QMessageBox>
#include <QTimer>

// C++ includes
#include <functional>

MainWindow::MainWindow()
{
    setWindowTitle(tr("Flohmarkt-WWS"));

    auto *mainWidget = new QWidget;
    setCentralWidget(mainWidget);
    m_layout = new QStackedLayout(mainWidget);

    m_welcomeWidget = new WelcomeWidget;
    m_layout->addWidget(m_welcomeWidget);
    connect(m_welcomeWidget, &WelcomeWidget::createVendorDocuments,
            this, &MainWindow::createVendorDocuments);
    connect(m_welcomeWidget, &WelcomeWidget::openVendorDocuments,
            this, &MainWindow::openVendorDocuments);
    connect(m_welcomeWidget, &WelcomeWidget::createFleaMarketDatabase,
            this, &MainWindow::createFleaMarketDatabase);
    connect(m_welcomeWidget, &WelcomeWidget::openFleaMarketDatabase,
            this, &MainWindow::openFleaMarketDatabase);

    m_vendorDocumentsWidget = new VendorDocumentsWidget;
    m_layout->addWidget(m_vendorDocumentsWidget);

    m_fleaMarketWidget = new FleaMarketWidget;
    connect(m_fleaMarketWidget, &FleaMarketWidget::sqlError, this, &MainWindow::sqlError);
    m_layout->addWidget(m_fleaMarketWidget);

    auto *fileMenu = menuBar()->addMenu(tr("Datei"));

    auto *createVendorDocumentsAction = fileMenu->addAction(tr("Verkäuferunterlagen erstellen"));
    connect(createVendorDocumentsAction, &QAction::triggered,
            this, &MainWindow::createVendorDocuments);

    auto *openVendorDocumentsAction = fileMenu->addAction(tr("Verkäuferunterlagen öffnen"));
    connect(openVendorDocumentsAction, &QAction::triggered, this, &MainWindow::openVendorDocuments);

    fileMenu->addSeparator();

    auto *createDatabaseAction = fileMenu->addAction(tr("Flohmarkt-Datenbank erstellen"));
    connect(createDatabaseAction, &QAction::triggered, this, &MainWindow::createFleaMarketDatabase);

    auto *openDatabaseAction = fileMenu->addAction(tr("Flohmarkt-Datenbank öffnen"));
    connect(openDatabaseAction, &QAction::triggered, this, &MainWindow::openFleaMarketDatabase);

    fileMenu->addSeparator();

    auto *quitAction = fileMenu->addAction(tr("Beenden"));
    connect(quitAction, &QAction::triggered, this, &QMainWindow::close);

    m_organizerViewMenu = menuBar()->addMenu(tr("Ansicht"));

    auto *showTabsMenu = m_organizerViewMenu->addMenu(tr("Angezeigte Tabs"));

    auto *showOrganizerOverviewTab = showTabsMenu->addAction(tr("Übersicht"));
    showOrganizerOverviewTab->setCheckable(true);
    showOrganizerOverviewTab->setChecked(true);
    showOrganizerOverviewTab->setData(Flohmarkt::OrganizerOverviewTab);
    connect(showOrganizerOverviewTab, &QAction::triggered,
            this, std::bind(&MainWindow::showOrganizerTab, this, showOrganizerOverviewTab));

    auto *showOrganizerProcessDataTab = showTabsMenu->addAction(tr("Daten erfassen"));
    showOrganizerProcessDataTab->setCheckable(true);
    showOrganizerProcessDataTab->setChecked(true);
    showOrganizerProcessDataTab->setData(Flohmarkt::OrganizerProcessDataTab);
    connect(showOrganizerProcessDataTab, &QAction::triggered,
            this, std::bind(&MainWindow::showOrganizerTab, this, showOrganizerProcessDataTab));

    auto *showOrganizerShoppingListTab = showTabsMenu->addAction(tr("Kassenzettel"));
    showOrganizerShoppingListTab->setCheckable(true);
    showOrganizerShoppingListTab->setChecked(true);
    showOrganizerShoppingListTab->setData(Flohmarkt::OrganizerShoppingListTab);
    connect(showOrganizerShoppingListTab, &QAction::triggered,
            this, std::bind(&MainWindow::showOrganizerTab, this, showOrganizerShoppingListTab));

    auto *showOrganizerItemListsTab = showTabsMenu->addAction(tr("Artikellisten"));
    showOrganizerItemListsTab->setCheckable(true);
    showOrganizerItemListsTab->setChecked(true);
    showOrganizerItemListsTab->setData(Flohmarkt::OrganizerItemListsTab);
    connect(showOrganizerItemListsTab, &QAction::triggered,
            this, std::bind(&MainWindow::showOrganizerTab, this, showOrganizerItemListsTab));

    m_organizerViewMenu->menuAction()->setVisible(false);

    auto *helpMenu = menuBar()->addMenu(tr("Hilfe"));
    auto *aboutAction = helpMenu->addAction(tr("Über Flohmarkt-WWS"));
    connect(aboutAction, &QAction::triggered,
            [this]
            {
                AboutDialog dialog(this);
                dialog.exec();
            });

    resize(1024, 786);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (! m_vendorDocumentsWidget->checkEdited()) {
        event->ignore();
        return;
    }

    event->accept();
}

void MainWindow::createVendorDocuments()
{
    if (! m_vendorDocumentsWidget->checkEdited()) {
        return;
    }

    m_vendorDocumentsWidget->clear(false);
    m_fleaMarketWidget->closeDatabase();
    m_layout->setCurrentWidget(m_vendorDocumentsWidget);
}

void MainWindow::openVendorDocuments()
{
    if (! m_vendorDocumentsWidget->checkEdited()) {
        return;
    }
    m_vendorDocumentsWidget->clear(false);

    if (! m_vendorDocumentsWidget->openDocuments()) {
        return;
    }
    m_fleaMarketWidget->closeDatabase();
    m_layout->setCurrentWidget(m_vendorDocumentsWidget);
}

void MainWindow::createFleaMarketDatabase()
{
    if (   ! m_vendorDocumentsWidget->clear()
        || ! m_fleaMarketWidget->createDatabase()) {

        if (m_layout->currentWidget() == m_fleaMarketWidget) {
            m_layout->setCurrentWidget(m_welcomeWidget);
        }
        return;
    }
    showOrganizerUi();
}

void MainWindow::openFleaMarketDatabase()
{
    if (   ! m_vendorDocumentsWidget->clear()
        || ! m_fleaMarketWidget->openDatabase()) {

        if (m_layout->currentWidget() == m_fleaMarketWidget) {
            m_layout->setCurrentWidget(m_welcomeWidget);
        }
        return;
    }
    showOrganizerUi();
}

void MainWindow::showOrganizerUi()
{
    m_layout->setCurrentWidget(m_fleaMarketWidget);
    m_organizerViewMenu->menuAction()->setVisible(true);
}

void MainWindow::sqlError(const QString &text)
{
    QTimer::singleShot(0, [this, text]
    {
        QMessageBox::critical(this, tr("SQL-Fehler"),
            tr("<p>Es ist ein SQL-Fehler aufgetreten! Das sollte nicht passieren! Sofern der "
               "Fehler reproduzierbar ist, bitte einen Bug-Report mit möglichst vielen "
               "Informationen über den Fehler schreiben!</p>"
               "<p>Die Fehlermeldung war:</p>") + text
               + tr("<p>Die Datenbank wird jetzt geschlossen.</p>"));
        m_fleaMarketWidget->closeDatabase();
        m_layout->setCurrentWidget(m_welcomeWidget);
    });
}

void MainWindow::showOrganizerTab(QAction *action)
{
    m_fleaMarketWidget->setTabVisible(static_cast<Flohmarkt::Tab>(action->data().toInt()),
                                      action->isChecked());
}
