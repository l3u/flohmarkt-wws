// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Local includes
#include "shared/Flohmarkt.h"

// Qt includes
#include <QMainWindow>

// Local classes
class WelcomeWidget;
class VendorDocumentsWidget;
class FleaMarketWidget;

// Qt classes
class QCloseEvent;
class QAction;
class QStackedLayout;
class QMenu;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow();

protected:
    void closeEvent(QCloseEvent *event) override;

private Q_SLOTS:
    void createVendorDocuments();
    void openVendorDocuments();
    void createFleaMarketDatabase();
    void openFleaMarketDatabase();
    void sqlError(const QString &text);
    void showOrganizerTab(QAction *action);

private: // Functions
    void showOrganizerUi();

private: // Variables
    QMenu *m_organizerViewMenu;

    QStackedLayout *m_layout;
    WelcomeWidget *m_welcomeWidget;
    VendorDocumentsWidget *m_vendorDocumentsWidget;
    FleaMarketWidget *m_fleaMarketWidget;

};

#endif // MAINWINDOW_H
