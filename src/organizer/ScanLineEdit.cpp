// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ScanLineEdit.h"

ScanLineEdit::ScanLineEdit(QWidget *parent) : QLineEdit(parent)
{
}

void ScanLineEdit::focusInEvent(QFocusEvent *event)
{
    QLineEdit::focusInEvent(event);
    Q_EMIT focusIn(true);
}

void ScanLineEdit::focusOutEvent(QFocusEvent *event)
{
    QLineEdit::focusOutEvent(event);
    Q_EMIT focusIn(false);
}
