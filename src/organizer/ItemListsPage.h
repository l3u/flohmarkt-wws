// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ITEMLISTSPAGE_H
#define ITEMLISTSPAGE_H

// Local includes
#include "AbstractListPage.h"
#include "Database.h"
#include "shared/Flohmarkt.h"

// Qt includes
#include <QHash>

// Local classes
class ItemsListModel;

// Qt classes
class QComboBox;
class QLabel;
class QTableView;
class QMenu;
class QPushButton;
class QAction;
class QFile;

class ItemListsPage : public AbstractListPage
{
    Q_OBJECT

public:
    explicit ItemListsPage(Database *database, QWidget *parent = nullptr);
    void clear();

Q_SIGNALS:
    void requestProcessItem(const Flohmarkt::ItemData &data) const;
    void requestProcessVendor(const Flohmarkt::VendorData &data) const;
    void requestAddToShoppingList(const Flohmarkt::ItemData &data) const;

private Q_SLOTS:
    void refresh();
    void itemChanged(const Flohmarkt::ItemData &data);
    void soldItemsEntered(const QVector<Flohmarkt::ItemData> &data);
    void vendorSelected(int index);
    void listSelected();
    void commissionChanged();
    void dataChanged();
    void showContextMenu(QPoint point);
    void processItem() const;
    void processVendor() const;
    void addToShoppingList() const;
    void printList();
    void saveList();
    void vendorChanged(const Flohmarkt::VendorData &data, Database::VendorChange change);

private: // Functions
    Flohmarkt::ItemData selectedItemData() const;
    void generateHtml(QFile &file);

private: // Variables
    Database *m_database;
    ItemsListModel *m_itemsListModel;
    QHash<QString, QString> m_listNumbers;

    QMenu *m_contextMenu;
    QAction *m_addToShoppingList;
    int m_selectedRow = -1;

    QPushButton *m_print;
    QPushButton *m_save;

    QComboBox *m_vendors;
    QComboBox *m_lists;
    QPushButton *m_processVendor;

    QTableView *m_itemsList;
    QLabel *m_missingItems;

    QLabel *m_totalItems;
    QLabel *m_soldItems;
    QLabel *m_sales;
    QLabel *m_commissionLabel;
    QLabel *m_commission;
    QLabel *m_profit;

};

#endif // ITEMLISTSPAGE_H
