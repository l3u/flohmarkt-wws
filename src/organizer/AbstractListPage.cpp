// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "AbstractListPage.h"

// Qt includes
#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QStandardPaths>

AbstractListPage::AbstractListPage(QWidget *parent) : QWidget(parent)
{
}

QString AbstractListPage::getSaveFileName(const QString &name, bool consecutive)
{
    QString path = m_lastSavePath.isEmpty()
        ? QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first()
        : m_lastSavePath;

    QString defaultFileName;

    if (consecutive) {
        int number = 1;
        while (true) {
            defaultFileName = tr("%1/%2_%3.html").arg(
                path, name, QString::number(number).rightJustified(4, QLatin1Char('0')));
            if (! QFileInfo::exists(defaultFileName)) {
                break;
            }
            number++;
        }
    } else {
        defaultFileName = tr("%1/%2.html").arg(path, name);
    }

    const auto fileName = QFileDialog::getSaveFileName(this,
        tr("%1 speichern").arg(name),
        defaultFileName,
        tr("HTML-Dateien (*.html *.htm);;Alle Dateien (*)"));

    if (fileName.isEmpty()) {
        return QString();
    }

    QFile file(fileName);
    if (! file.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(this,
            tr("%1 speichern").arg(name),
            tr("<p>Die Datei <kbd>%1</kbd> konnte nicht zum Schreiben geöffnet werden!</p>"
               "<p>Bitte die Zugriffsrechte überprüfen!</p>").arg(
               QDir::toNativeSeparators(fileName).toHtmlEscaped()));
        return QString();
    }
    file.close();

    return fileName;
}

void AbstractListPage::setLastSavePath(const QString &fileName)
{
    m_lastSavePath = QFileInfo(fileName).absoluteDir().absolutePath();
}

const QLocale &AbstractListPage::locale() const
{
    return m_locale;
}
