// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef OVERVIEWPAGE_H
#define OVERVIEWPAGE_H

// Qt includes
#include <QWidget>
#include <QLocale>

// Local classes
class Database;

// Qt classes
class QLineEdit;
class QSpinBox;
class QPushButton;
class QLabel;

class OverviewPage : public QWidget
{
    Q_OBJECT

public:
    explicit OverviewPage(Database *database, QWidget *parent = nullptr);

private Q_SLOTS:
    void refresh();
    void setEventName();
    void setCommission();
    void updateOverview();

private: // Variables
    Database *m_database;
    QLocale m_locale;

    QLineEdit *m_eventName;
    QPushButton *m_setEventName;
    QSpinBox *m_commission;
    QPushButton *m_setCommission;

    double m_salesValue = 0.0;
    QLabel *m_vendorsCount;
    QLabel *m_articlesCount;
    QLabel *m_articlesSold;
    QLabel *m_sales;
    QLabel *m_commissionLabel;

};

#endif // OVERVIEWPAGE_H
