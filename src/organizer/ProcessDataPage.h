// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef PROCESSDATAPAGE_H
#define PROCESSDATAPAGE_H

// Local includes
#include "shared/Flohmarkt.h"

// Qt includes
#include <QWidget>
#include <QLocale>

// Local classes
class Database;
class ScanWidget;

// Qt classes
class QStackedLayout;
class QLabel;
class QPushButton;
class QCheckBox;

class ProcessDataPage : public QWidget
{
    Q_OBJECT

public:
    explicit ProcessDataPage(Database *database, QWidget *parent = nullptr);
    void clear();
    void startScan();

public Q_SLOTS:
    void refresh();
    void scannedItemData(const Flohmarkt::ItemData &data);
    void scannedVendorData(const Flohmarkt::VendorData &data);

private Q_SLOTS:
    void scannedInvalidData();

    void vendorChanged(const Flohmarkt::VendorData &data);
    void itemChanged(const Flohmarkt::ItemData &data);
    void soldItemsEntered(const QVector<Flohmarkt::ItemData> &data);

    bool registerVendor();
    void deleteVendor();

    bool registerItem();
    void deleteItem();
    void markItemAsSold();
    void markItemAsNotSold();

    void datasetAltered(const QString &name, const QString &m_listNumber);

private: // Variables
    Database *m_database;
    QLocale m_locale;
    ScanWidget *m_scanWidget;

    Flohmarkt::Qr::Type m_currentType;
    Flohmarkt::VendorData m_vendorData;
    Flohmarkt::ItemData m_itemData;

    QStackedLayout *m_dataLayout;

    QWidget *m_noDataWidget;

    QWidget *m_vendorWidget;
    QLabel *m_vendorName;
    QLabel *m_listNumber;

    QWidget *m_itemWidget;
    QLabel *m_itemVendor;
    QLabel *m_listInfo;
    QLabel *m_itemDescription;
    QLabel *m_itemSize;
    QLabel *m_itemPrice;

    QLabel *m_dbStatus;

    QStackedLayout *m_actionsLayout;

    QWidget *m_noActionsWidget;

    QWidget *m_vendorActionsWidget;
    QPushButton *m_registerVendor;
    QPushButton *m_deleteVendor;

    QWidget *m_itemActionsWidget;
    QPushButton *m_registerItem;
    QPushButton *m_deleteItem;
    QPushButton *m_markItemAsSold;
    QPushButton *m_markItemAsNotSold;

    QCheckBox *m_autoRegister;
    bool m_skipAutoRegister = false;

};

#endif // PROCESSDATAPAGE_H
