// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ShoppingListPage.h"
#include "ScanWidget.h"
#include "ShoppingListModel.h"
#include "BusyHelper.h"
#include "shared/IconButton.h"
#include "shared/HtmlGenerator.h"
#include "shared/TemporaryFile.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QTableView>
#include <QMessageBox>
#include <QMenu>
#include <QHeaderView>
#include <QLabel>
#include <QHBoxLayout>
#include <QTimer>
#include <QFile>
#include <QDir>
#include <QLocale>
#include <QDesktopServices>

static const auto s_style = QStringLiteral(R"END(
body {
    font-size: 12pt;
}

table {
    border-spacing: 0;
}

table th:last-child {
    text-align: right;
}

th, td {
    padding: 0.2em 0.8em 0.2em;
    text-align: left;
}

table tr td {
    border-top: 1px solid black;
}

table tr td:nth-child(3) {
    text-align: right;
}

table tr:nth-last-child(2) td {
    font-weight: bold;
    font-size: larger;
}

table tr:nth-last-child(1) td {
    border: none;
    font-size: smaller;
}

table tr:nth-last-child(2) td:first-child,
table tr:nth-last-child(1) td:first-child {
    text-align: right;
}

table tr:nth-last-child(2) td:last-child,
table tr:nth-last-child(1) td:last-child {
    text-align: left;
}

table tr:last-child td {
    border-bottom: 1px solid black;
}

@media print {
    @page {
        margin: 1.5cm 2cm;
    }
}

)END");

ShoppingListPage::ShoppingListPage(Database *database, QWidget *parent)
    : AbstractListPage(parent),
      m_database(database)
{
    connect(m_database, &Database::databaseChanged, this, &ShoppingListPage::commissionChanged);
    connect(m_database, &Database::commissionChanged, this, &ShoppingListPage::commissionChanged);
    connect(m_database, &Database::itemChanged, this, &ShoppingListPage::itemChanged);
    connect(m_database, &Database::vendorChanged, this, &ShoppingListPage::vendorChanged);

    auto *layout = new QVBoxLayout(this);

    auto *controlLayout = new QHBoxLayout;
    layout->addLayout(controlLayout);
    m_print = new IconButton(tr("Drucken"), QStringLiteral("document-print"), true);
    connect(m_print, &QPushButton::clicked, this, &ShoppingListPage::printShoppingList);
    m_print->setEnabled(false);
    controlLayout->addWidget(m_print);

    m_save = new IconButton(tr("Speichern als"), QStringLiteral("document-save-as"), true);
    connect(m_save, &QPushButton::clicked, this, &ShoppingListPage::saveShoppingList);
    m_save->setEnabled(false);
    controlLayout->addWidget(m_save);

    controlLayout->addStretch();

    m_scanWidget = new ScanWidget;
    connect(m_scanWidget, &ScanWidget::scannedVendorData,
            this, &ShoppingListPage::scannedVendorData);
    connect(m_scanWidget, &ScanWidget::scannedItemData,
            this, &ShoppingListPage::scannedItemData);
    layout->addWidget(m_scanWidget);

    auto *itemsBox = new QGroupBox(tr("Kassenzettel"));
    layout->addWidget(itemsBox);
    auto *itemsBoxLayout = new QVBoxLayout(itemsBox);

    m_view = new QTableView;
    m_view->setContextMenuPolicy(Qt::CustomContextMenu);
    m_view->setFocusPolicy(Qt::NoFocus);
    connect(m_view, &QTableView::customContextMenuRequested,
            this, &ShoppingListPage::showContextMenu);
    itemsBoxLayout->addWidget(m_view);
    m_model = new ShoppingListModel(this);
    connect(m_model, &ShoppingListModel::dataChanged, this, &ShoppingListPage::updateSum);
    m_view->setModel(m_model);

    auto *sumBoxLayoutWrapper = new QHBoxLayout;
    itemsBoxLayout->addLayout(sumBoxLayoutWrapper);
    auto *sumBoxLayout = new QGridLayout;
    sumBoxLayoutWrapper->addLayout(sumBoxLayout);
    sumBoxLayoutWrapper->addStretch();

    sumBoxLayout->addWidget(new QLabel(tr("<b>Summe:</b>")), 0, 0);
    m_sum = new QLabel;
    m_sum->setAlignment(Qt::AlignRight);
    sumBoxLayout->addWidget(m_sum, 0, 1);
    m_commissionLabel = new QLabel;
    sumBoxLayout->addWidget(m_commissionLabel, 1, 0);
    m_commission = new QLabel;
    m_commission->setAlignment(Qt::AlignRight);
    sumBoxLayout->addWidget(m_commission, 1, 1);

    m_enter = new IconButton(tr("Kassenzettel\nbuchen"),
                                         QStringLiteral("database-add"));
    connect(m_enter, &QPushButton::clicked, this, &ShoppingListPage::enterShoppingList);
    sumBoxLayoutWrapper->addWidget(m_enter);

    m_contextMenu = new QMenu(this);
    auto *deleteAction = m_contextMenu->addAction(tr("Artikel entfernen"));
    connect(deleteAction, &QAction::triggered, this, &ShoppingListPage::removeItem);
}

void ShoppingListPage::startScan()
{
    m_scanWidget->startScan();
}

void ShoppingListPage::scannedItemData(const Flohmarkt::ItemData &data)
{
    const auto [ itemStatus, success ] = m_database->itemStatus(data.itemId);
    if (! success) {
        return;
    }

    switch (itemStatus) {
    case Database::ItemNotPresent:
        QMessageBox::warning(this, tr("Artikel hinzufügen"),
            tr("Der gescannte Artikel wurde bisher nicht registriert und kann deswegen nicht zum "
               "Kassenzettel hinzugefügt werden!"));
        return;
    case Database::ItemSold:
        QMessageBox::warning(this, tr("Artikel hinzufügen"),
                             tr("Der gescannte Artikel wurde bereits als „verkauft“ markiert!"));
        return;
    case Database::ItemAvailable:
        break;
    }

    if (m_model->itemPresent(data.itemId)) {
        QMessageBox::information(this, tr("Artikel hinzufügen"),
            tr("Der Artikel „%1“ ist bereits auf dem Kassenzettel!").arg(data.description));
        return;
    }

    m_model->addItem(data);
    m_view->resizeColumnsToContents();
    m_view->scrollToBottom();
}

void ShoppingListPage::scannedVendorData()
{
    QMessageBox::warning(this, tr("Artikel hinzufügen"),
                         tr("Der gescannte Code war kein Artikel-, sondern ein Verkäufer-Code!"));
}

void ShoppingListPage::showContextMenu(QPoint point)
{
    const auto index = m_view->indexAt(point);
    if (! index.isValid()) {
        return;
    }

    m_selectedRow = index.row();
    point.setY(point.y() + m_view->horizontalHeader()->height());
    m_contextMenu->exec(m_view->mapToGlobal(point));
}

void ShoppingListPage::removeItem()
{
    m_model->removeItem(m_selectedRow);
}

void ShoppingListPage::commissionChanged()
{
    m_commissionLabel->setText(tr("Darin enthaltene Provision (%1\u202F%)").arg(
                                  m_database->commission()));
    updateSum();
}

void ShoppingListPage::updateSum()
{
    const auto sum = m_model->sum() / 100.0;
    m_sum->setText(locale().toCurrencyString(sum));
    m_commission->setText(locale().toCurrencyString(sum / 100.0 * m_database->commission()));

    const auto enable = m_model->rowCount(QModelIndex()) > 0;
    m_print->setEnabled(enable);
    m_save->setEnabled(enable);
    m_enter->setEnabled(enable);
}

void ShoppingListPage::itemChanged(const Flohmarkt::ItemData &data, Database::ItemChange change)
{
    if (m_model->itemPresent(data.itemId)) {
        if (change == Database::ItemMarkedAsSold) {
            QTimer::singleShot(0, [this, data]
            {
                QMessageBox::warning(this, tr("Artikel als verkauft markiert"),
                    tr("Der Artikel „%1“ wurde als „verkauft“ markiert. Er wird vom aktuellen "
                       "Kassenzettel entfernt!").arg(data.description));
            });
            m_model->removeItem(data.itemId);
        } else if (change == Database::ItemDeleted) {
            QTimer::singleShot(0, [this, data]
            {
                QMessageBox::warning(this, tr("Artikel gelöscht"),
                    tr("Der Artikel „%1“ wurde gelöscht. Er wird vom aktuellen Kassenzettel "
                       "entfernt!").arg(data.description));
            });
            m_model->removeItem(data.itemId);
        }
    }
}

void ShoppingListPage::vendorChanged(const Flohmarkt::VendorData &data,
                                     Database::VendorChange change)
{
    if (change != Database::VendorDeleted) {
        return;
    }

    auto removedItems = m_model->removeVendorItems(data.id);
    if (removedItems > 0) {
        QTimer::singleShot(0, [this, data, removedItems]
        {
            QMessageBox::warning(this, tr("Verkäufer gelöscht"),
                tr("Der Verkäufer „%1“ wurde gelöscht. Der aktuelle Kassenzettel enthielt %2 "
                   "Artikel dieses Verkäufers. Diese Artikel wurden entfernt!").arg(
                   data.name, QString::number(removedItems)));
        });
    }
}

void ShoppingListPage::enterShoppingList()
{
    const auto &items = m_model->items();
    Q_ASSERT(items.count() > 0);
    if (items.count() == 0) {
        // This should not happen but would cause a segfault in the database.
        // So, just to be sure:
        return;
    }

    BusyHelper busyHelper(this);

    QHash<QString, QVector<Flohmarkt::ItemData>> vendorItems;
    for (const auto &item : items) {
        if (! vendorItems.contains(item.vendorId)) {
            vendorItems[item.vendorId] = { };
        }
        vendorItems[item.vendorId].append(item);
    }

    QHash<QString, QVector<Flohmarkt::ItemData>>::const_iterator i = vendorItems.constBegin();
    while (i != vendorItems.constEnd()) {
        if (! m_database->enterSoldItems(i.value())) {
            return;
        }
        i++;
    }

    m_model->clear();
    busyHelper.reset();
    QMessageBox::information(this, tr("Kassenzettel buchen"), tr("Kassenzettel gebucht!"));

    m_scanWidget->startScan();
}

void ShoppingListPage::saveShoppingList()
{
    const auto fileName = getSaveFileName(tr("Kassenzettel"), true);
    if (fileName.isEmpty()) {
        return;
    }

    QFile file(fileName);
    generateHtml(file);
    file.close();
    setLastSavePath(fileName);

    if (QMessageBox::question(this,
            tr("Kassenzettel speichern"),
            tr("<p>Kassenzettel gespeichert als <kbd>%1</kbd></p>"
               "<p><b>Soll der Kassenzettel jetzt gebucht werden?</b>").arg(
               QDir::toNativeSeparators(fileName).toHtmlEscaped()),
            QMessageBox::Yes | QMessageBox::No,
            QMessageBox::Yes)

        == QMessageBox::Yes) {

        enterShoppingList();
    }

    m_scanWidget->startScan();
}

void ShoppingListPage::printShoppingList()
{
    TemporaryFile file;
    file.open();
    generateHtml(file);
    QDesktopServices::openUrl(file.url());

    if (QMessageBox::question(this,
            tr("Kassenzettel drucken"),
            tr("<p>Es wird nun eine HTML-Datei mit dem Kassenzettel erstellt und mit dem "
               "Standard-Webbrowser geöffnet. Damit kann die Liste dann gedruckt werden.</p>"
               "<p>Der Dateiname ist:<br>"
               "<kbd><a href=\"%1\">%2</a></kbd></p>"
               "<p>Die Datei wird automatisch gelöscht, sobald dieser Dialog geschlossen wird.</p>"
               "<p><b>Soll der Kassenzettel jetzt gebucht werden?</b></p>").arg(
               file.htmlUrl(), file.displayName()),
               QMessageBox::Yes | QMessageBox::No,
               QMessageBox::Yes)

        == QMessageBox::Yes) {

        enterShoppingList();
    }

    m_scanWidget->startScan();
}

void ShoppingListPage::generateHtml(QFile &file)
{
    HtmlGenerator html(&file);
    html.setTitle(tr("Kassenzettel"));
    html.setStyle(s_style);
    html.startHtml();

    html << "<h1>" << m_database->eventName().toHtmlEscaped() << "</h1>\n\n"

         << "<h2>Kassenzettel</h2>\n\n"

         << "<table>\n"
         << "<thead>\n"
         << "<tr>\n"
         << "<th>" << tr("Beschreibung") << "</th>"
         << "<th>" << tr("Größe") << "</th>"
         << "<th>" << tr("Preis") << "</th>\n"
         << "</tr>\n"
         << "</thead>\n"
         << "<tbody>\n";

    const QLocale locale;
    for (const auto &item : m_model->items()) {
        html << "<tr>"
             << "<td>" << item.description.toHtmlEscaped() << "</td>"
             << "<td>" << item.size.toHtmlEscaped() << "</td>"
             << "<td>" << locale.toCurrencyString(item.price / 100.0) << "</td>"
             << "</tr>\n";
    }

    html << "<tr><td colspan=\"3\">&nbsp;</td></tr>\n"
         << "<tr><td colspan=\"2\">" << tr("Summe:") << "</td>"
         << "<td>" << m_sum->text() << "</td></tr>\n"
         << "<tr><td colspan=\"2\">" << m_commissionLabel->text().toHtmlEscaped() << "</td>"
         << "<td>" << m_commission->text() << "</td></tr>\n"
         << "</tbody>\n"
         << "</table>\n\n";

    html.finishHtml();
}
