// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ItemListsPage.h"
#include "ItemsListModel.h"
#include "shared/IconButton.h"
#include "shared/HtmlGenerator.h"
#include "shared/TemporaryFile.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QComboBox>
#include <QLabel>
#include <QTableView>
#include <QGridLayout>
#include <QMenu>
#include <QHeaderView>
#include <QPushButton>
#include <QFile>
#include <QDesktopServices>
#include <QMessageBox>

// C++ includes
#include <cmath>

static const auto s_style = QStringLiteral(R"END(
body {
    font-size: 12pt;
}

h1 + p {
    font-size: larger;
    margin-top: -0.6em;
}

table {
    margin-top: 1em;
    border-spacing: 0;
}

th, td {
    padding: 0.2em 0.8em 0.2em;
    text-align: left;
}

table thead tr th {
    border-bottom: 1px solid black;
}

table tr:first-child td {
    border: none;
}

table tr td {
    border-top: 1px solid black;
}

table:last-child tr:last-child td:first-child {
    font-weight: bold;
}

@media print {
    @page {
        margin: 1.5cm 2cm;
    }
}

)END");

ItemListsPage::ItemListsPage(Database *database, QWidget *parent)
    : AbstractListPage(parent),
      m_database(database)
{
    connect(m_database, &Database::databaseChanged, this, &ItemListsPage::refresh);
    connect(m_database, &Database::commissionChanged, this, &ItemListsPage::commissionChanged);
    connect(m_database, &Database::vendorChanged, this, &ItemListsPage::vendorChanged);
    connect(m_database, &Database::itemChanged, this, &ItemListsPage::itemChanged);
    connect(m_database, &Database::soldItemsEntered, this, &ItemListsPage::soldItemsEntered);

    connect(m_database, &Database::vendorRenamed,
            [this](const QString &id, const QString &name)
            {
                m_vendors->setItemText(m_vendors->findData(id), name);
            });

    connect(m_database, &Database::listNumberRenamed,
            [this](const QString &id, const QString &name)
            {
                m_lists->setItemText(m_lists->findData(id), name);
            });

    m_contextMenu = new QMenu(this);
    m_addToShoppingList = m_contextMenu->addAction(tr("Zum aktuellen Kassenzettel hinzufügen"));
    connect(m_addToShoppingList, &QAction::triggered,
            this, &ItemListsPage::addToShoppingList);
    m_contextMenu->addSeparator();
    auto *process = m_contextMenu->addAction(tr("Bearbeiten"));
    connect(process, &QAction::triggered, this, &ItemListsPage::processItem);

    auto *layout = new QVBoxLayout(this);

    auto *controlLayout = new QHBoxLayout;
    layout->addLayout(controlLayout);

    m_print = new IconButton(tr("Drucken"), QStringLiteral("document-print"), true);
    connect(m_print, &QPushButton::clicked, this, &ItemListsPage::printList);
    m_print->setEnabled(false);
    controlLayout->addWidget(m_print);

    m_save = new IconButton(tr("Speichern als"), QStringLiteral("document-save-as"), true);
    connect(m_save, &QPushButton::clicked, this, &ItemListsPage::saveList);
    m_save->setEnabled(false);
    controlLayout->addWidget(m_save);

    controlLayout->addStretch();

    auto *vendorBox = new QGroupBox(tr("Verkäufer/Liste"));
    layout->addWidget(vendorBox);
    auto *vendorBoxLayout = new QGridLayout(vendorBox);

    m_vendors = new QComboBox;
    connect(m_vendors, &QComboBox::currentIndexChanged, this, &ItemListsPage::vendorSelected);
    auto *vendorLabel = new QLabel(tr("Verkäufer:"));
    vendorLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    vendorBoxLayout->addWidget(vendorLabel, 0, 0);
    vendorBoxLayout->addWidget(m_vendors, 0, 1);

    m_lists = new QComboBox;
    connect(m_lists, &QComboBox::currentIndexChanged, this, &ItemListsPage::listSelected);
    auto *listLabel = new QLabel(tr("Liste:"));
    vendorLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    vendorBoxLayout->addWidget(listLabel, 1, 0);
    vendorBoxLayout->addWidget(m_lists, 1, 1);

    m_processVendor = new QPushButton(tr("Verkäufer\nbearbeiten"));
    m_processVendor->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    m_processVendor->setEnabled(false);
    connect(m_processVendor, &QPushButton::clicked, this, &ItemListsPage::processVendor);
    vendorBoxLayout->addWidget(m_processVendor, 0, 2, 2, 1);

    auto *itemsBox = new QGroupBox(tr("Artikelliste"));
    auto *itemsBoxLayout = new QVBoxLayout(itemsBox);

    m_itemsList = new QTableView;
    m_itemsList->setFocusPolicy(Qt::NoFocus);
    m_itemsList->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_itemsList, &QTableView::customContextMenuRequested,
            this, &ItemListsPage::showContextMenu);
    m_itemsListModel = new ItemsListModel(m_database, this);
    connect(m_itemsListModel, &QAbstractTableModel::dataChanged,
            m_itemsList, &QTableView::resizeColumnsToContents);
    connect(m_itemsListModel, &QAbstractTableModel::dataChanged,
            this, &ItemListsPage::dataChanged);
    m_itemsList->setModel(m_itemsListModel);
    itemsBoxLayout->addWidget(m_itemsList);

    m_missingItems =  new QLabel;
    m_missingItems->setWordWrap(true);
    itemsBoxLayout->addWidget(m_missingItems);

    layout->addWidget(itemsBox);

    auto *salesOverviewBox = new QGroupBox(tr("Verkaufsübersicht"));
    auto *salesOverviewBoxLayoutWrapper = new QHBoxLayout(salesOverviewBox);
    auto *salesOverviewBoxLayout = new QGridLayout;
    salesOverviewBoxLayoutWrapper->addLayout(salesOverviewBoxLayout);
    layout->addWidget(salesOverviewBox);

    salesOverviewBoxLayout->addWidget(new QLabel(tr("Artikel gesamt:")), 0, 0);
    m_totalItems = new QLabel;
    salesOverviewBoxLayout->addWidget(m_totalItems, 0, 1);

    salesOverviewBoxLayout->addWidget(new QLabel(tr("Davon verkauft:")), 1, 0);
    m_soldItems = new QLabel;
    salesOverviewBoxLayout->addWidget(m_soldItems, 1, 1);

    salesOverviewBoxLayout->addWidget(new QLabel(QStringLiteral("      ")), 0, 2); // Spacer

    salesOverviewBoxLayout->addWidget(new QLabel(tr("Umsatz:")), 0, 3);
    m_sales = new QLabel;
    salesOverviewBoxLayout->addWidget(m_sales, 0, 4);

    m_commissionLabel = new QLabel;
    salesOverviewBoxLayout->addWidget(m_commissionLabel, 1, 3);
    m_commission = new QLabel;
    salesOverviewBoxLayout->addWidget(m_commission, 1, 4);

    salesOverviewBoxLayoutWrapper->addStretch();
    salesOverviewBoxLayoutWrapper->addWidget(new QLabel(tr("<b>Gewinn:</b>")));
    m_profit = new QLabel;
    salesOverviewBoxLayoutWrapper->addWidget(m_profit);
    salesOverviewBoxLayoutWrapper->addStretch();
}

void ItemListsPage::clear()
{
    m_vendors->clear();
    m_itemsListModel->clear();
}

void ItemListsPage::vendorChanged(const Flohmarkt::VendorData &data, Database::VendorChange change)
{
    switch (change) {
    case Database::VendorRegistered:
        m_listNumbers[data.id] = data.listNumber.isEmpty() ? data.id : data.listNumber;
        m_vendors->addItem(data.name, data.id);
        m_vendors->model()->sort(0);
        m_lists->addItem(m_listNumbers.value(data.id), data.id);
        m_lists->model()->sort(0);
        break;

    case Database::VendorDeleted:
        m_vendors->removeItem(m_vendors->findData(data.id));
        m_lists->removeItem(m_lists->findData(data.id));
        m_listNumbers.remove(data.id);
    }
}

void ItemListsPage::itemChanged(const Flohmarkt::ItemData &data)
{
    if (data.vendorId == m_vendors->currentData().toString()) {
        m_itemsListModel->refresh();
    }
}

void ItemListsPage::soldItemsEntered(const QVector<Flohmarkt::ItemData> &data)
{
    Q_ASSERT(data.count() > 0);
    if (data.count() == 0) {
        // This should not happen, but who knows?!
        return;
    }

    if (data.first().vendorId == m_vendors->currentData().toString()) {
        m_itemsListModel->refresh();
    }
}

void ItemListsPage::vendorSelected(int index)
{
    int listsIndex = -1;
    if (index == -1) {
        m_itemsListModel->clear();
    } else {
        const auto id = m_vendors->currentData().toString();
        listsIndex = m_lists->findData(id);
        m_itemsListModel->setVendorId(id);
    }

    m_lists->blockSignals(true);
    m_lists->setCurrentIndex(listsIndex);
    m_lists->blockSignals(false);

    m_processVendor->setEnabled(index != -1);
}

void ItemListsPage::listSelected()
{
    m_vendors->setCurrentIndex(m_vendors->findData(m_lists->currentData().toString()));
}

void ItemListsPage::refresh()
{
    const auto [vendors, success] = m_database->vendors();
    if (! success) {
        return;
    }

    for (const auto &info : vendors) {
        vendorChanged(info, Database::VendorRegistered);
    }

    commissionChanged();
}

void ItemListsPage::commissionChanged()
{
    m_commissionLabel->setText(tr("Provision (%1\u202F%):").arg(m_database->commission()));
    dataChanged();
}

void ItemListsPage::dataChanged()
{
    const auto count = m_itemsListModel->rowCount(QModelIndex());

    m_totalItems->setText(QString::number(count));
    m_soldItems->setText(QString::number(m_itemsListModel->soldItems()));

    const auto sales = m_itemsListModel->salesVolume();
    const auto commission = std::round(sales / 100.0 * m_database->commission());
    const auto profit = sales - commission;

    m_sales->setText(locale().toCurrencyString(sales / 100.0));
    m_commission->setText(locale().toCurrencyString(commission / 100.0));
    m_profit->setText(tr("<b>%1</b>").arg(locale().toCurrencyString(profit / 100.0)));

    const auto enabled = (count > 0 && (m_vendors->currentIndex() != -1));
    m_print->setEnabled(enabled);
    m_save->setEnabled(enabled);

    if (enabled) {
        QVector<int> listNumbers;
        for (const auto &item : m_itemsListModel->items()) {
            listNumbers.append(item.listNumber);
        }

        QVector<int> missing;
        for (int i = 1; i <= listNumbers.last(); i++) {
            if (! listNumbers.contains(i)) {
                missing.append(i);
            }
        }

        if (missing.count() > 0) {
            QStringList list;
            for (int number : missing) {
                list.append(QString::number(number));
            }
            m_missingItems->setText(tr("Die Artikel mit den folgenden Nummern fehlen: ")
                                    + list.join(QStringLiteral(", ")));
            m_missingItems->show();
        } else {
            m_missingItems->hide();
        }
    }
}

void ItemListsPage::showContextMenu(QPoint point)
{
    const auto index = m_itemsList->indexAt(point);
    if (! index.isValid()) {
        return;
    }

    m_selectedRow = index.row();
    point.setY(point.y() + m_itemsList->horizontalHeader()->height());
    m_addToShoppingList->setEnabled(! m_itemsListModel->itemData(m_selectedRow).sold);
    m_contextMenu->exec(m_itemsList->mapToGlobal(point));
}

Flohmarkt::ItemData ItemListsPage::selectedItemData() const
{
    const auto &data = m_itemsListModel->itemData(m_selectedRow);
    return Flohmarkt::ItemData {
        m_vendors->currentData().toString(), // QString vendorId
        data.id,                             // QString itemId
        data.listNumber,                     // int listNumber
        data.description,                    // QString description
        data.size,                           // QString size
        data.price                           // int price
    };
}

void ItemListsPage::processItem() const
{
    Q_EMIT requestProcessItem(selectedItemData());
}

void ItemListsPage::processVendor() const
{
    const auto id = m_vendors->currentData().toString();
    Q_EMIT requestProcessVendor(Flohmarkt::VendorData {
        id,                       // QString id
        m_vendors->currentText(), // QString name
        m_listNumbers.value(id)   // QString listNumber
    });
}

void ItemListsPage::addToShoppingList() const
{
    Q_EMIT requestAddToShoppingList(selectedItemData());
}

void ItemListsPage::printList()
{
    TemporaryFile file;
    file.open();
    generateHtml(file);
    QDesktopServices::openUrl(file.url());

    QMessageBox::information(this,
        tr("Verkaufsliste drucken"),
        tr("<p>Es wird nun eine HTML-Datei mit der Verkaufsliste erstellt und mit dem "
           "Standard-Webbrowser geöffnet. Damit kann die Liste dann gedruckt werden.</p>"
           "<p>Der Dateiname ist:<br>"
           "<kbd><a href=\"%1\">%2</a></kbd></p>"
           "<p>Die Datei wird automatisch gelöscht, sobald dieser Dialog geschlossen wird."
           "</p>").arg(file.htmlUrl(), file.displayName()));
}

void ItemListsPage::saveList()
{
    const auto fileName = getSaveFileName(tr("%1 (Listennummer %2)").arg(m_vendors->currentText(),
                                                                         m_lists->currentText()));
    if (fileName.isEmpty()) {
        return;
    }

    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    generateHtml(file);
    file.close();
    setLastSavePath(fileName);
}

void ItemListsPage::generateHtml(QFile &file)
{
    HtmlGenerator html(&file);
    const auto title = tr("Verkaufsliste für %1").arg(m_vendors->currentText());
    html.setTitle(title);
    html.setStyle(s_style);
    html.startHtml();

    const auto count = m_itemsListModel->rowCount(QModelIndex());
    const auto sold = m_itemsListModel->soldItems();

    html << "<h1>" << title << "</h1>\n\n"

         << "<p>" << tr("Listennummer: %1").arg(m_lists->currentText()) << "</p>\n\n";

    if (sold < count) {
        html << "<h2>" << tr("Nicht verkaufte Artikel") << "</h2>\n\n"
             << "<table>\n"

             << "<thead>\n"
             << "<tr>"
             << "<th>" << tr("Nummer") << "</th>"
             << "<th>" << tr("Beschreibung") << "</th>"
             << "<th>" << tr("Größe") << "</th>"
             << "<th>" << tr("Preis") << "</th>"
             << "<th>" << tr("Erfasst") << "</th>"
             << "</thead>\n"

             << "<tbody>\n";

        for (const auto &item : m_itemsListModel->items()) {
            if (item.sold) {
                continue;
            }
            html << "<tr>"
                 << "<td>" << item.listNumber << "</td>"
                 << "<td>" << item.description << "</td>"
                 << "<td>" << (item.size.isEmpty() ? tr("–") : item.size) << "</td>"
                 << "<td>" << locale().toCurrencyString(item.price / 100.0) << "</td>"
                 << "<td></td>"
                 << "</tr>\n";
        }

        html << "</tbody>\n"
             << "</table>\n\n";
    }

    if (sold > 0) {
        html << "<h2>" << tr("Verkaufte Artikel") << "</h2>\n\n"
             << "<table>\n"

             << "<thead>\n"
             << "<tr>"
             << "<th>" << tr("Nummer") << "</th>"
             << "<th>" << tr("Beschreibung") << "</th>"
             << "<th>" << tr("Größe") << "</th>"
             << "<th>" << tr("Preis") << "</th>"
             << "</thead>\n"

             << "<tbody>\n";

        for (const auto &item : m_itemsListModel->items()) {
            if (! item.sold) {
                continue;
            }
            html << "<tr>"
                 << "<td>" << item.listNumber << "</td>"
                 << "<td>" << item.description << "</td>"
                 << "<td>" << (item.size.isEmpty() ? tr("–") : item.size) << "</td>"
                 << "<td>" << locale().toCurrencyString(item.price / 100.0) << "</td>"
                 << "</tr>\n";
        }

        html << "</tbody>\n"
             << "</table>\n\n";
    }

    html << "<h2>" << tr("Zusammenfassung") << "</h2>\n\n"

         << "<table>\n"

         << "<tr>"
         << "<td>" << tr("Erfasste Artikel:") << "</td>"
         << "<td>" << count << "</td>"
         << "</tr>\n"

         << "<tr>"
         << "<td>" << tr("Verkauft:") << "</td>"
         << "<td>" << sold << "</td>"
         << "</tr>\n"

         << "<tr>"
         << "<td>" << tr("Nicht verkauft:") << "</td>"
         << "<td>" << (count - sold) << "</td>"
         << "</tr>\n"

         << "</table>\n\n"

         << "<table>\n"

         << "<tr>"
         << "<td>" << tr("Summe verkaufte Artikel:") << "</td>"
         << "<td>" << m_sales->text() << "</td>"
         << "</tr>\n"

         << "<tr>"
         << "<td>" << m_commissionLabel->text() << "</td>"
         << "<td>" << m_commission->text() << "</td>"
         << "</tr>\n"

         << "<tr>"
         << "<td>" << tr("Gewinn:") << "</td>"
         << "<td>" << m_profit->text() << "</td>"
         << "</tr>\n"

         << "</table>\n\n";

    html.finishHtml();
}
