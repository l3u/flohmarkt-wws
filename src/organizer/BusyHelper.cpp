// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "BusyHelper.h"

// Qt includes
#include <QWidget>
#include <QApplication>

BusyHelper::BusyHelper(QWidget *parent, bool disable)
    : QObject(parent), m_parent(parent), m_disable(disable)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    if (m_disable) {
        m_parent->setEnabled(false);
    }
}

BusyHelper::~BusyHelper()
{
    reset();
}

void BusyHelper::reset()
{
    if (m_resetCalled) {
        return;
    }

    QApplication::restoreOverrideCursor();
    if (m_disable) {
        m_parent->setEnabled(true);
    }

    m_resetCalled = true;
}
