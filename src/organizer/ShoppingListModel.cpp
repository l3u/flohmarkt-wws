// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ShoppingListModel.h"

// Qt includes
#include <QDebug>

// C++ includes
#include <utility>

ShoppingListModel::ShoppingListModel(QObject *parent) : QAbstractTableModel(parent)
{
}

int ShoppingListModel::rowCount(const QModelIndex &) const
{
    return m_items.count();
}

int ShoppingListModel::columnCount(const QModelIndex &) const
{
    return 3;
}

QVariant ShoppingListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    switch (orientation) {
    case Qt::Horizontal:
        if (section == 0) {
            return tr("Beschreibung");
        } else if (section == 1) {
            return tr("Größe");
        } else if (section == 2) {
            return tr("Preis");
        }
        break;
    case Qt::Vertical:
        return section + 1;
    }

    // We can't reach here
    return QVariant();
}

QVariant ShoppingListModel::data(const QModelIndex &index, int role) const
{
    if (! index.isValid()) {
        return QVariant();
    }

    const auto column = index.column();

    if (role == Qt::TextAlignmentRole) {
        if (column == 2) {
            return int(Qt::AlignRight | Qt::AlignVCenter);
        } else if (column == 3) {
            return Qt::AlignCenter;
        } else {
            return QVariant();
        }
    }

    const auto row = index.row();
    if (row >= m_items.count() || role != Qt::DisplayRole) {
        return QVariant();
    }

    if (column == 0) {
        return m_items.at(row).description;
    } else if (column == 1) {
        return m_items.at(row).size.isEmpty() ? tr("–") : m_items.at(row).size;
    } else if (column == 2) {
        if (role == Qt::DisplayRole) {
            return m_locale.toCurrencyString(m_items.at(row).price / 100.0);
        } else {
            return m_items.at(row).price;
        }
    }

    // We can't reach here
    return QVariant();
}

Qt::ItemFlags ShoppingListModel::flags(const QModelIndex &) const
{
    return Qt::ItemIsEnabled;
}

bool ShoppingListModel::itemPresent(const QString &id) const
{
    return m_ids.contains(id);
}

void ShoppingListModel::addItem(const Flohmarkt::ItemData &data)
{
    const auto row = m_items.count();
    beginInsertRows(QModelIndex(), row, row);
    m_items.append(data);
    m_ids.append(data.itemId);
    m_sum += data.price;
    endInsertRows();
    const auto modelIndex = index(row, 0);
    Q_EMIT dataChanged(modelIndex, modelIndex, { Qt::DisplayRole });
}

void ShoppingListModel::removeItem(int row)
{
    beginRemoveRows(QModelIndex(), row, row);
    m_sum -= m_items.at(row).price;
    m_items.removeAt(row);
    m_ids.removeAt(row);
    endRemoveRows();
    const auto modelIndex = index(row, 0);
    Q_EMIT dataChanged(modelIndex, modelIndex, { Qt::DisplayRole });
}

void ShoppingListModel::removeItem(const QString &id)
{
    removeItem(m_ids.indexOf(id));
}

int ShoppingListModel::sum() const
{
    return m_sum;
}

int ShoppingListModel::removeVendorItems(const QString &id)
{
    QVector<QString> ids;
    for (const auto &item : std::as_const(m_items)) {
        if (item.vendorId == id) {
            ids.append(item.itemId);
        }
    }

    for (const auto &id : std::as_const(ids)) {
        removeItem(id);
    }

    return ids.count();
}

const QVector<Flohmarkt::ItemData> &ShoppingListModel::items() const
{
    return m_items;
}

void ShoppingListModel::clear()
{
    const auto count = m_items.count();
    beginRemoveRows(QModelIndex(), 0, count - 1);
    m_items.clear();
    m_ids.clear();
    m_sum = 0;
    endRemoveRows();
    const auto fistModelIndex = index(0, 0);
    const auto lastModelIndex = index(count, 0);
    Q_EMIT dataChanged(fistModelIndex, lastModelIndex, { Qt::DisplayRole });
}
