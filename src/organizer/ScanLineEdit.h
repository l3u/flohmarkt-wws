// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCANLINEEDIT_H
#define SCANLINEEDIT_H

// Qt includes
#include <QLineEdit>

// Qt classes
class QFocusEvent;

class ScanLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    explicit ScanLineEdit(QWidget *parent = nullptr);

Q_SIGNALS:
    void focusIn(bool state);

protected:
    void focusInEvent(QFocusEvent *event) override;
    void focusOutEvent(QFocusEvent *event) override;

};

#endif // SCANLINEEDIT_H
