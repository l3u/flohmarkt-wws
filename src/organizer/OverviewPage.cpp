// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "OverviewPage.h"
#include "Database.h"
#include "BusyHelper.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QMessageBox>
#include <QTimer>
#include <QGridLayout>

// C++ includes
#include <functional>

OverviewPage::OverviewPage(Database *database, QWidget *parent)
    : QWidget(parent),
      m_database(database)
{
    connect(m_database, &Database::databaseChanged, this, &OverviewPage::refresh);
    connect(m_database, &Database::vendorChanged, this, &OverviewPage::updateOverview);
    connect(m_database, &Database::itemChanged, this, &OverviewPage::updateOverview);
    connect(m_database, &Database::soldItemsEntered, this, &OverviewPage::updateOverview);

    auto *layout = new QVBoxLayout(this);

    auto *settingsBox = new QGroupBox(tr("Einstellungen"));
    auto *settingsBoxLayout = new QGridLayout(settingsBox);
    layout->addWidget(settingsBox);

    settingsBoxLayout->addWidget(new QLabel(tr("Veranstaltungsname:")), 0, 0);
    m_eventName = new QLineEdit;
    settingsBoxLayout->addWidget(m_eventName, 0, 1);
    m_setEventName = new QPushButton(tr("Speichern"));
    connect(m_eventName, &QLineEdit::textEdited,
            m_setEventName, std::bind(&QWidget::setEnabled, m_setEventName, true));
    connect(m_setEventName, &QPushButton::clicked, this, &OverviewPage::setEventName);

    settingsBoxLayout->addWidget(m_setEventName, 0, 2);

    settingsBoxLayout->addWidget(new QLabel(tr("Provision:")), 1,  0);
    m_commission = new QSpinBox;
    m_commission->setMinimum(0);
    m_commission->setMaximum(999);
    m_commission->setSuffix(tr("\u202F%"));
    settingsBoxLayout->addWidget(m_commission, 1, 1);

    m_setCommission = new QPushButton(tr("Speichern"));
    connect(m_commission, &QSpinBox::valueChanged,
            m_setCommission, std::bind(&QWidget::setEnabled, m_setCommission, true));
    connect(m_setCommission, &QPushButton::clicked, this, &OverviewPage::setCommission);

    settingsBoxLayout->addWidget(m_setCommission, 1, 2);

    auto *overviewBox = new QGroupBox(tr("Übersicht"));
    auto *overviewWrapper = new QHBoxLayout(overviewBox);
    overviewWrapper->addStretch();
    auto *overviewLayout = new QGridLayout;
    overviewWrapper->addLayout(overviewLayout);
    overviewWrapper->addStretch();
    layout->addWidget(overviewBox);

    overviewLayout->addWidget(new QLabel(tr("Registrierte Verkäufer:")), 0, 0);
    m_vendorsCount = new QLabel;
    overviewLayout->addWidget(m_vendorsCount, 0, 1);

    overviewLayout->addWidget(new QLabel(tr("Registrierte Artikel:")), 1, 0);
    m_articlesCount = new QLabel;
    overviewLayout->addWidget(m_articlesCount, 1, 1);

    overviewLayout->addWidget(new QLabel(tr("Verkaufte Artikel:")), 2, 0);
    m_articlesSold = new QLabel;
    overviewLayout->addWidget(m_articlesSold, 2, 1);

    overviewLayout->addWidget(new QLabel(tr("Umsatz:")), 3, 0);
    m_sales = new QLabel;
    overviewLayout->addWidget(m_sales, 3, 1);

    overviewLayout->addWidget(new QLabel(tr("Provision:")), 4, 0);
    m_commissionLabel = new QLabel;
    overviewLayout->addWidget(m_commissionLabel, 4, 1);

    layout->addStretch();
}

void OverviewPage::refresh()
{
    m_eventName->setText(m_database->eventName());
    m_commission->setValue(m_database->commission());
    m_setEventName->setEnabled(false);
    m_setCommission->setEnabled(false);
    updateOverview();
}

void OverviewPage::updateOverview()
{
    const auto [ overview, success ] = m_database->overview();
    if (! success) {
        return;
    }

    m_vendorsCount->setText(QString::number(overview.vendors));
    m_articlesCount->setText(QString::number(overview.items));
    m_articlesSold->setText(QString::number(overview.sold));
    m_salesValue = overview.sales / 100.0;
    m_sales->setText(m_locale.toCurrencyString(m_salesValue));
    m_commissionLabel->setText(m_locale.toCurrencyString(
        m_salesValue / 100.0 * m_commission->value()));
}

void OverviewPage::setEventName()
{
    BusyHelper busyHelper(this);
    m_eventName->setText(m_eventName->text().simplified());
    if (! m_database->setEventName(m_eventName->text())) {
        return;
    }

    busyHelper.reset();
    QMessageBox::information(this, tr("Veranstaltungsname speichern"),
                             tr("Veranstaltungsname gespeichert!"));
    m_setEventName->setEnabled(false);
}

void OverviewPage::setCommission()
{
    BusyHelper busyHelper(this);
    if (! m_database->setCommission(m_commission->value())) {
        return;
    }

    busyHelper.reset();
    QMessageBox::information(this, tr("Provision speichern"), tr("Provision gespeichert!"));
    m_setCommission->setEnabled(false);

    m_commissionLabel->setText(m_locale.toCurrencyString(
        m_salesValue / 100.0 * m_commission->value()));
}
