// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef DATABASE_H
#define DATABASE_H

// Local includes
#include "shared/Flohmarkt.h"

// Qt includes
#include <QObject>
#include <QSqlDatabase>

// Qt classes
class QLockFile;

class Database : public QObject
{
    Q_OBJECT

public: // Enums
    enum Error {
        NoError,
        CouldNotOpen
    };

    enum ItemStatus {
        ItemNotPresent,
        ItemAvailable,
        ItemSold
    };

    enum VendorChange {
        VendorRegistered,
        VendorDeleted
    };

    enum ItemChange {
        ItemRegistered,
        ItemDeleted,
        ItemMarkedAsSold,
        ItemMarkedAsNotSold
    };

public: // Structs
    template<typename T>
    struct TSuccess
    {
        T data;
        bool success;
    };

    struct ItemVendorData
    {
        bool registered = false;
        QString name;
        QString listNumber;
    };

    struct ItemListData
    {
        QString id;
        int listNumber;
        QString description;
        QString size;
        int price;
        bool sold;
    };

    struct Overview
    {
        int vendors;
        int items;
        int sold;
        int sales;
    };

public: // Functions
    explicit Database(QObject *parent);
    bool createDatabase(const QString &fileName);
    Error openDatabase(const QString &dbFile);
    void closeDatabaseConnection();
    const QString &errorMessage() const;
    const QString &dbFile() const;
    const QString &dbPath() const;
    QString checkForLockFile(const QString &dbFile) const;

    bool setEventName(const QString &name);
    const QString &eventName() const;

    int commission() const;
    bool setCommission(int value);

    TSuccess<int> soldItems();

    TSuccess<bool> vendorRegistered(const QString &id);
    bool registerVendor(Flohmarkt::VendorData data);
    bool deleteVendor(const Flohmarkt::VendorData &data);

    TSuccess<ItemStatus> itemStatus(const QString &id);
    TSuccess<ItemVendorData> itemVendorData(const QString &vendorId);
    bool registerItem(const Flohmarkt::ItemData &data);
    bool deleteItem(const Flohmarkt::ItemData &data);
    bool markItemAsSold(const Flohmarkt::ItemData &data, bool sold);
    bool enterSoldItems(const QVector<Flohmarkt::ItemData> &data);

    TSuccess<QVector<Flohmarkt::VendorData>> vendors();
    TSuccess<QVector<ItemListData>> itemsForVendor(const QString &id);

    TSuccess<Overview> overview();

Q_SIGNALS:
    void sqlError(const QString &text);
    void databaseChanged();
    void commissionChanged();
    void vendorChanged(const Flohmarkt::VendorData &data, VendorChange change);
    void itemChanged(const Flohmarkt::ItemData &data, ItemChange change);
    void soldItemsEntered(const QVector<Flohmarkt::ItemData> &data);
    void vendorRenamed(const QString &id, const QString &name);
    void listNumberRenamed(const QString &id, const QString &name);
    void datasetAltered(const QString &name, const QString &listNumber);

private: // Structs
    struct VendorData
    {
        QString id;
        QString name;
        QString listNumber;
    };

private: // Functions
    bool connectDatabase(const QString &fileName);
    void updateDbFileInfo();
    bool checkIfDbIsLocked();
    bool lockDatabase();
    QString lockFileName(const QString &dbFile) const;

    bool startTransaction();
    bool commitTransaction();
    bool rollbackTransaction(QSqlQuery &query);
    QString sqlErrorText() const;
    QString sqlErrorText(const QSqlQuery &query) const;
    void emitSqlError(const QString &text);
    bool queryPrepare(QSqlQuery &query, const QString &text);
    bool queryExec(QSqlQuery &query);
    bool queryExec(QSqlQuery &query, const QString &text);
    bool queryExecBatch(QSqlQuery &query);

    template<typename SqlFunction>
    bool writeHelper(SqlFunction sqlFunction)
    {
        if (! startTransaction()) {
            return false;
        }

        QSqlQuery query(m_db);
        try {
            sqlFunction(query);
            commitTransaction();
        } catch (...) {
            rollbackTransaction(query);
            return false;
        }

        return true;
    }

    template<typename SqlFunction>
    void readHelper(SqlFunction sqlFunction, bool &success)
    {
        try {
            QSqlQuery query(m_db);
            sqlFunction(query);
            success = true;
        } catch (...) {
            success = false;
        }
    }

private: // Variables
    QSqlDatabase m_db;
    QLockFile *m_lockFile = nullptr;
    bool m_emitSqlErrors = false;
    QString m_errorMessage;
    QString m_dbFileName;
    QString m_dbFilePath;

    QString m_eventName;
    int m_commission = 0;

};

#endif // DATABASE_H
