// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ProcessDataPage.h"
#include "Database.h"
#include "ScanWidget.h"
#include "BusyHelper.h"
#include "shared/IconButton.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStackedLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QApplication>
#include <QTimer>
#include <QCheckBox>

ProcessDataPage::ProcessDataPage(Database *database, QWidget *parent)
    : QWidget(parent), m_database(database)
{
    connect(m_database, &Database::vendorChanged, this, &ProcessDataPage::vendorChanged);
    connect(m_database, &Database::itemChanged, this, &ProcessDataPage::itemChanged);
    connect(m_database, &Database::soldItemsEntered, this, &ProcessDataPage::soldItemsEntered);
    connect(m_database, &Database::datasetAltered, this, &ProcessDataPage::datasetAltered);

    auto *layout = new QVBoxLayout(this);

    m_scanWidget = new ScanWidget;
    connect(m_scanWidget, &ScanWidget::scannedInvalidData,
            this, &ProcessDataPage::scannedInvalidData);
    connect(m_scanWidget, &ScanWidget::scannedVendorData,
            this, &ProcessDataPage::scannedVendorData);
    connect(m_scanWidget, &ScanWidget::scannedItemData,
            this, &ProcessDataPage::scannedItemData);
    layout->addWidget(m_scanWidget);

    // Data box
    auto *dataBox = new QGroupBox(tr("Gescannte Daten"));
    auto *dataBoxLayout = new QHBoxLayout(dataBox);
    m_dataLayout = new QStackedLayout;
    dataBoxLayout->addStretch();
    dataBoxLayout->addLayout(m_dataLayout);
    dataBoxLayout->addStretch();
    layout->addWidget(dataBox);

    // No data display
    m_noDataWidget = new QWidget;
    auto *noDataWidgetLayout = new QVBoxLayout(m_noDataWidget);
    auto *noDataLabel = new QLabel(tr("Keine Daten verfügbar"));
    noDataLabel->setAlignment(Qt::AlignCenter);
    noDataWidgetLayout->addWidget(noDataLabel);
    m_dataLayout->addWidget(m_noDataWidget);

    // Vendor info display

    m_vendorWidget = new QWidget;
    auto *vendorWidgetLayoutWrapper = new QVBoxLayout(m_vendorWidget);
    auto *vendorWidgetLayout = new QGridLayout;
    vendorWidgetLayoutWrapper->addLayout(vendorWidgetLayout);

    auto *vendorLabel = new QLabel(tr("Verkäuferinfo"));
    vendorLabel->setAlignment(Qt::AlignHCenter);
    vendorLabel->setStyleSheet(QStringLiteral("QLabel { font-weight: bold; font-size: %1pt }").arg(
        (int) double(vendorLabel->font().pointSize()) * 1.2));
    vendorWidgetLayout->addWidget(vendorLabel, 0, 0, 1, 2);
    vendorWidgetLayout->addWidget(new QLabel(tr("Name:")), 1, 0);

    m_vendorName = new QLabel;
    vendorWidgetLayout->addWidget(m_vendorName, 1, 1);
    vendorWidgetLayout->addWidget(new QLabel(tr("Listennummer bzw. ID:")), 2, 0);

    m_listNumber = new QLabel;
    vendorWidgetLayout->addWidget(m_listNumber, 2, 1);

    vendorWidgetLayoutWrapper->addStretch();
    m_dataLayout->addWidget(m_vendorWidget);

    // Item data display

    m_itemWidget = new QWidget;
    auto itemWidgetLayoutWrapper = new QVBoxLayout(m_itemWidget);
    auto *itemWidgetLayout = new QGridLayout;
    itemWidgetLayoutWrapper->addLayout(itemWidgetLayout);

    auto *itemLabel = new QLabel(tr("Artikelinfo"));
    itemLabel->setAlignment(Qt::AlignHCenter);
    itemLabel->setStyleSheet(QStringLiteral("QLabel { font-weight: bold; font-size: %1pt }").arg(
        (int) double(itemLabel->font().pointSize()) * 1.2));
    itemWidgetLayout->addWidget(itemLabel, 0, 0, 1, 2);
    itemWidgetLayout->addWidget(new QLabel(tr("Verkäufer:")), 1, 0);

    m_itemVendor = new QLabel;
    itemWidgetLayout->addWidget(m_itemVendor, 1, 1);
    itemWidgetLayout->addWidget(new QLabel(tr("Listennummer:")), 2, 0);

    m_listInfo = new QLabel;
    itemWidgetLayout->addWidget(m_listInfo, 2, 1);
    itemWidgetLayout->addWidget(new QLabel(tr("Beschreibung:")), 3, 0);

    m_itemDescription = new QLabel;
    m_itemDescription->setWordWrap(true);
    itemWidgetLayout->addWidget(m_itemDescription, 3, 1);
    itemWidgetLayout->addWidget(new QLabel(tr("Größe:")), 4, 0);

    m_itemSize = new QLabel;
    m_itemSize->setWordWrap(true);
    itemWidgetLayout->addWidget(m_itemSize, 4, 1);
    itemWidgetLayout->addWidget(new QLabel(tr("Preis:")), 5, 0);

    m_itemPrice = new QLabel;
    itemWidgetLayout->addWidget(m_itemPrice, 5, 1);

    itemWidgetLayoutWrapper->addStretch();
    m_dataLayout->addWidget(m_itemWidget);

    // Database status
    auto *statusBox = new QGroupBox(tr("Datenbank-Status"));
    auto *statusLayout = new QVBoxLayout(statusBox);
    layout->addWidget(statusBox);
    m_dbStatus = new QLabel(tr("Kein Status verfügbar"));
    m_dbStatus->setAlignment(Qt::AlignHCenter);
    statusLayout->addWidget(m_dbStatus);

    // Item Actions

    auto *actionsBox = new QGroupBox(tr("Aktionen"));
    actionsBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    auto *actionsBoxLayout = new QVBoxLayout(actionsBox);
    m_actionsLayout = new QStackedLayout;
    actionsBoxLayout->addLayout(m_actionsLayout);
    layout->addWidget(actionsBox);

    // No actions display
    m_noActionsWidget = new QWidget;
    auto *noActionsLayout = new QVBoxLayout(m_noActionsWidget);
    auto *noActionsLabel = new QLabel(tr("Keine Aktionen verfügbar"));
    noActionsLabel->setAlignment(Qt::AlignCenter);
    noActionsLayout->addWidget(noActionsLabel);
    m_actionsLayout->addWidget(m_noActionsWidget);

    // Vendor actions

    m_vendorActionsWidget = new QWidget;
    auto *vendorActionsLayout = new QHBoxLayout(m_vendorActionsWidget);
    m_actionsLayout->addWidget(m_vendorActionsWidget);

    m_registerVendor = new IconButton(tr("Verkäufer\nregistrieren"),
                                      QStringLiteral("database-add"));
    connect(m_registerVendor, &QPushButton::clicked, this, &ProcessDataPage::registerVendor);
    vendorActionsLayout->addWidget(m_registerVendor);

    m_deleteVendor = new IconButton(tr("Verkäufer\nlöschen"), QStringLiteral("database-delete"));
    connect(m_deleteVendor, &QPushButton::clicked, this, &ProcessDataPage::deleteVendor);
    vendorActionsLayout->addWidget(m_deleteVendor);

    // Item actions

    m_itemActionsWidget = new QWidget;
    auto itemsLayout = new QHBoxLayout(m_itemActionsWidget);
    m_actionsLayout->addWidget(m_itemActionsWidget);

    m_registerItem = new IconButton(tr("Artikel\nregistrieren"), QStringLiteral("database-add"));
    connect(m_registerItem, &QPushButton::clicked, this, &ProcessDataPage::registerItem);
    itemsLayout->addWidget(m_registerItem);

    m_deleteItem = new IconButton(tr("Artikel\nlöschen"), QStringLiteral("database-delete"));
    connect(m_deleteItem, &QPushButton::clicked, this, &ProcessDataPage::deleteItem);
    itemsLayout->addWidget(m_deleteItem);

    m_markItemAsSold = new IconButton(tr("Artikel als\nverkauft markieren"),
                                      QStringLiteral("sold"));
    connect(m_markItemAsSold, &QPushButton::clicked, this, &ProcessDataPage::markItemAsSold);
    itemsLayout->addWidget(m_markItemAsSold);
    m_markItemAsNotSold = new IconButton(tr("Artikel als noch nicht\nverkauft markieren"),
                                         QStringLiteral("not-sold"));

    connect(m_markItemAsNotSold, &QPushButton::clicked, this, &ProcessDataPage::markItemAsNotSold);
    itemsLayout->addWidget(m_markItemAsNotSold);

    m_autoRegister = new QCheckBox(tr("Neue Verkäufer und Artikel automatisch registrieren"));
    m_autoRegister->setChecked(true);
    actionsBoxLayout->addWidget(m_autoRegister);
}

void ProcessDataPage::scannedInvalidData()
{
    m_dataLayout->setCurrentWidget(m_noDataWidget);
    m_actionsLayout->setCurrentWidget(m_noActionsWidget);
    m_dbStatus->setText(tr("Keine Daten verfügbar"));
}

void ProcessDataPage::scannedVendorData(const Flohmarkt::VendorData &data)
{
    m_vendorData = data;
    m_currentType = Flohmarkt::Qr::VendorCode;
    refresh();
}

void ProcessDataPage::scannedItemData(const Flohmarkt::ItemData &data)
{
    m_itemData = data;
    m_currentType = Flohmarkt::Qr::ItemCode;
    refresh();
}

void ProcessDataPage::clear()
{
    scannedInvalidData();
}

void ProcessDataPage::startScan()
{
    m_scanWidget->startScan();
}

void ProcessDataPage::vendorChanged(const Flohmarkt::VendorData &data)
{
    if (m_currentType == Flohmarkt::Qr::VendorCode && m_vendorData.id == data.id) {
        refresh();
    }
}

void ProcessDataPage::itemChanged(const Flohmarkt::ItemData &data)
{
    if (m_currentType == Flohmarkt::Qr::ItemCode && m_itemData.itemId == data.itemId) {
        refresh();
    }
}

void ProcessDataPage::soldItemsEntered(const QVector<Flohmarkt::ItemData> &data)
{
    if (m_currentType != Flohmarkt::Qr::ItemCode) {
        return;
    }

    for (const auto &item : data) {
        if (item.itemId == m_itemData.itemId) {
            refresh();
            break;
        }
    }
}

void ProcessDataPage::refresh()
{
    // Fetch possibly present data about the vendor or item from the database

    bool vendorRegistered = false;
    Database::ItemVendorData itemVendorData;
    Database::ItemStatus itemStatus;

    QString vendorId;
    if (m_currentType == Flohmarkt::Qr::VendorCode) {
        vendorId = m_vendorData.id;
    } else if (m_currentType == Flohmarkt::Qr::ItemCode) {
        vendorId = m_itemData.vendorId;
    }

    const auto [ registered, success ] = m_database->vendorRegistered(vendorId);
    if (! success) {
        return;
    }
    vendorRegistered = registered;

    if (m_currentType == Flohmarkt::Qr::ItemCode) {
        const auto [ vendorData, success1 ] = m_database->itemVendorData(vendorId);
        if (! success) {
            return;
        }
        itemVendorData = vendorData;
        const auto [ itemData, success2 ] = m_database->itemStatus(m_itemData.itemId);
        if (! success) {
            return;
        }
        itemStatus = itemData;
    }

    // Possibly auto-register the dataset

    bool autoRegistered = false;

    if (m_autoRegister->isChecked() && ! m_skipAutoRegister) {
        if (m_currentType == Flohmarkt::Qr::VendorCode && ! vendorRegistered) {
            if (registerVendor()) {
                autoRegistered = true;
                vendorRegistered = true;
            } else {
                return;
            }
        } else if (m_currentType == Flohmarkt::Qr::ItemCode && vendorRegistered
                   && itemStatus == Database::ItemNotPresent) {

            if (registerItem()) {
                autoRegistered = true;
                itemStatus = Database::ItemAvailable;
            } else {
                return;
            }
        }
    }

    // Display the data

    if (m_currentType == Flohmarkt::Qr::VendorCode) {
        m_vendorName->setText(m_vendorData.name);
        m_listNumber->setText(m_vendorData.listNumber.isEmpty() ? m_vendorData.id
                                                                : m_vendorData.listNumber);
        m_dataLayout->setCurrentWidget(m_vendorWidget);
        m_actionsLayout->setCurrentWidget(m_vendorActionsWidget);

        if (autoRegistered) {
            m_dbStatus->setText(tr("<p style=\"color: green;\">Der Verkäufer wurde automatisch "
                                   "registriert!</p>"));
        } else {
            m_dbStatus->setText(vendorRegistered
                ? tr("<p style=\"color: green;\">Der Verkäufer ist bereits registriert</p>")
                : tr("<p style=\"color: red;\">Der Verkäufer ist noch nicht registriert</p>"));
        }
        m_registerVendor->setEnabled(! vendorRegistered);
        m_deleteVendor->setEnabled(vendorRegistered);

    } else if (m_currentType == Flohmarkt::Qr::ItemCode) {
        m_itemVendor->setText(vendorRegistered ? itemVendorData.name : m_itemData.vendorId);
        m_listInfo->setText(tr("%1   Artikel: %2").arg(
            vendorRegistered ? itemVendorData.listNumber : m_itemData.vendorId,
            QString::number(m_itemData.listNumber)));
        m_itemDescription->setText(m_itemData.description);
        m_itemSize->setText(m_itemData.size.isEmpty() ? tr("–") : m_itemData.size);
        m_itemPrice->setText(m_locale.toCurrencyString(m_itemData.price / 100.0));
        m_dataLayout->setCurrentWidget(m_itemWidget);
        m_actionsLayout->setCurrentWidget(m_itemActionsWidget);

        if (! vendorRegistered) {
            m_dbStatus->setText(tr(
                "<p style=\"color: red;\">Der Verkäufer des Artikels ist bisher nicht registriert. "
                "Der Artikel kann nicht verarbeitet werden!</p>"));
            m_registerItem->setEnabled(false);
            m_deleteItem->setEnabled(false);
            m_markItemAsSold->setEnabled(false);
            m_markItemAsNotSold->setEnabled(false);

        } else {
            if (itemStatus == Database::ItemNotPresent) {
                m_dbStatus->setText(
                    tr("<p style=\"color: red;\">Der Artikel ist noch nicht registriert</p>"));
            } else {
                if (autoRegistered) {
                    m_dbStatus->setText(tr("<p><span style=\"color: green;\">Der Artikel wurde "
                                           "automatisch registriert!"));
                } else {
                    m_dbStatus->setText(itemStatus == Database::ItemSold
                        ? tr("<p><span style=\"color: green;\">Der Artikel ist bereits registriert"
                            "</span> und als <b>„verkauft“</b> markiert")
                        : tr("<p><span style=\"color: green;\">Der Artikel ist bereits registriert"
                            "</span> und als <b>„nicht verkauft“</b> markiert"));
                }
            }
            m_registerItem->setEnabled(itemStatus == Database::ItemNotPresent);
            m_deleteItem->setEnabled(itemStatus != Database::ItemNotPresent);
            m_markItemAsSold->setEnabled(itemStatus == Database::ItemAvailable);
            m_markItemAsNotSold->setEnabled(itemStatus == Database::ItemSold);
        }
    }
}

bool ProcessDataPage::registerVendor()
{
    BusyHelper busyHelper(this, true);
    if (! m_database->registerVendor(m_vendorData)) {
        return false;
    }
    startScan();
    return true;
}

void ProcessDataPage::deleteVendor()
{
    if (QMessageBox::question(this, tr("Verkäufer löschen"),
            tr("<p>Soll der Verkäufer „%1“ wirklich gelöscht werden?</p>"
               "<p>Es werden auch alle registrierten Artikel des Verkäufers gelöscht!</p>"
               "<p><b>Dieser Vorgang ist nicht umkehrbar!</b></p>").arg(
               m_vendorData.name),
            QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::No) {

        return;
    }

    m_skipAutoRegister = true;
    BusyHelper busyHelper(this, true);
    if (! m_database->deleteVendor(m_vendorData)) {
        m_skipAutoRegister = false;
        return;
    }
    startScan();
    m_skipAutoRegister = false;
}

bool ProcessDataPage::registerItem()
{
    BusyHelper busyHelper(this, true);
    if (! m_database->registerItem(m_itemData)) {
        return false;
    }
    startScan();
    return true;
}

void ProcessDataPage::deleteItem()
{
    if (QMessageBox::question(this, tr("Artikel löschen"),
            tr("Soll der Artikel „%1“ wirklich gelöscht werden?").arg(m_itemData.description),
            QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::No) {

        return;
    }

    const auto [ status, success ] = m_database->itemStatus(m_itemData.itemId);
    if (! success) {
        return;
    }
    if (status == Database::ItemSold && QMessageBox::question(this, tr("Artikel löschen"),
            tr("<p>Der Artikel „%1“ wurde bereits als „verkauft“ markiert. Wenn er gelöscht wird, "
               "dann wird er aus der Artikelliste des Verkäufers entfernt, und nicht mehr "
               "berücksichtigt!</p>"
               "<p>Soll der Artikel „%1“ wirklich gelöscht werden?</p>").arg(
               m_itemData.description),
            QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::No) {

        return;
    }

    m_skipAutoRegister = true;
    BusyHelper busyHelper(this, true);
    if (! m_database->deleteItem(m_itemData)) {
        m_skipAutoRegister = false;
        return;
    }
    startScan();
    m_skipAutoRegister = false;
}

void ProcessDataPage::markItemAsSold()
{
    BusyHelper busyHelper(this, true);
    if (! m_database->markItemAsSold(m_itemData, true)) {
        return;
    }
    startScan();
}

void ProcessDataPage::markItemAsNotSold()
{
    BusyHelper busyHelper(this, true);
    if (! m_database->markItemAsSold(m_itemData, false)) {
        return;
    }
    startScan();
}

void ProcessDataPage::datasetAltered(const QString &name, const QString &listNumber)
{
    QTimer::singleShot(0,
        [this, name, listNumber]
        {
            QMessageBox::information(this,
                tr("Verkäufer registrieren"),
                tr("<p>Die Datenbank enthielt bereits einen anderen Verkäufer mit einem "
                "identischen Namen bzw. einer identischen Listennummer. Der Übersicht wegen "
                "wurde der Datensatz vor dem Speichern angepasst (ggf. auch der "
                "Bestehende).</p>"
                "<p>Folgender Datensatz wurde gespeichert:</p>"
                "<table>"
                "<tr><td>Name:</td><td>%1</td></tr>"
                "<tr><td>Listennummer:</td><td>%2</td>"
                "</table>").arg(
                name, listNumber.isEmpty() ? tr("(keine angegeben)") : listNumber));
        });
}
