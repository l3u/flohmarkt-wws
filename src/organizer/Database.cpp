// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "Database.h"
#include "shared/Logging.h"

// Qt includes
#include <QFileInfo>
#include <QSqlQuery>
#include <QDir>
#include <QSqlError>
#include <QLockFile>
#include <QVariantList>
#include <QDebug>
#include <QStringList>
#include <QRegularExpression>

// C++ includes
#include <utility>

// Metadata keys and values
static const QString s_dbIdKey(QLatin1String("db id"));
static const QString s_dbId(QLatin1String("Flohmarkt-WWS database"));
static const QString s_dbVersionKey(QLatin1String("db version"));
static constexpr int s_dbVersion = 1;
static const QString s_eventNameKey(QLatin1String("event name"));
static const QString s_commissionKey(QLatin1String("commission"));

// This one is only used to check if we can write to the database in a transaction that
// is rolled back afterwards. So this key will never be actually written to a database.
static const QString s_writeTestKey(QLatin1String("write test"));

// Internally used connection name
static const QString s_connectionName(QLatin1String("SQLiteDB"));

// Regular expression to get consecutive numbers
const QRegularExpression s_consecutiveNumber(QStringLiteral("(.+) \\(\\d\\)$"));

Database::Database(QObject *parent) : QObject(parent)
{
}

bool Database::createDatabase(const QString &fileName)
{
    if (! connectDatabase(fileName)) {
        return false;
    }

    if (checkIfDbIsLocked()) {
        closeDatabaseConnection();
        m_errorMessage = tr(
            "<p><b>Unerwarteter Fehler:</b> Die Datenbank ist gesperrt</p>"
            "<p>Das tritt auf, wenn eine Datenbank auf einer Windows-Netzwerk-Freigabe (bzw. "
            "einem Samba-Share/CIFS-Mount) angelegt werden soll; das ist nicht möglich.</p>"
            "<p>Ansonsten sollte das nicht passieren, sofern kein anderer Prozess die Datenbank "
            "gesperrt hat!</p>");
        return false;
    }

    if (! lockDatabase()) {
        closeDatabaseConnection();
        m_errorMessage = tr(
            "<p><b>Unerwarteter Fehler:</b> Die Lock-Datei „%1“ konnte nicht erstellt werden. "
            "Bitte die Zugriffsrechte des Verzeichnisses „%2“ überprüfen!</p>").arg(
            QDir::toNativeSeparators(m_dbFilePath),
            QDir::toNativeSeparators(lockFileName(m_dbFileName)));
        return false;
    }

    if (! startTransaction()) {
        return false;
    }

    QSqlQuery query(m_db);

    m_eventName = tr("Flohmarkt");
    m_commission = 0;

    try {
        // Setup the tables
        queryExec(query, QStringLiteral("CREATE TABLE config(\n"
                                        "    key TEXT,\n"
                                        "    value TEXT\n"
                                        ")"));
        queryExec(query, QStringLiteral("CREATE TABLE vendors(\n"
                                        "    id TEXT PRIMARY KEY,\n"
                                        "    name TEXT,\n"
                                        "    listNumber TEXT\n"
                                        ")"));
        queryExec(query, QStringLiteral("CREATE TABLE items(\n"
                                        "    id TEXT PRIMARY KEY,\n"
                                        "    vendor TEXT,\n"
                                        "    listNumber INT,\n"
                                        "    description TEXT,\n"
                                        "    size TEXT,\n"
                                        "    price INT,\n"
                                        "    sold INT\n"
                                        ")"));

        // Insert the default config keys and values
        queryPrepare(query, QStringLiteral("INSERT INTO config(key, value) VALUES(?, ?)"));
        query.addBindValue(QVariantList { s_dbIdKey,
                                          s_dbVersionKey,
                                          s_eventNameKey,
                                          s_commissionKey });
        query.addBindValue(QVariantList { s_dbId,
                                          s_dbVersion,
                                          m_eventName,
                                          QString::number(m_commission) });
        queryExecBatch(query);

        commitTransaction();

    } catch (...) {
        const auto errorText = sqlErrorText(query);
        rollbackTransaction(query);
        m_errorMessage = tr(
            "<p>Beim Erstellen der Datenbankstruktur ist ein Fehler aufgetreten. Das sollte nicht "
            "passieren! Evtl. wurde die Datei während des Erstellens von einem anderen Prozess "
            "verändert?</p>") + errorText;
        return false;
    }

    m_emitSqlErrors = true;
    Q_EMIT databaseChanged();
    return true;
}

Database::Error Database::openDatabase(const QString &dbFile)
{
    // Check if it's a file and if we can read it
    const QFileInfo fileInfo = QFileInfo(dbFile);
    if (! fileInfo.exists()) {
        m_errorMessage = tr("<p>Die Datei „%1“ konnte nicht gefunden werden.</p>").arg(
                            QDir::toNativeSeparators(dbFile));
        return Error::CouldNotOpen;
    }
    if (! (fileInfo.isFile() || fileInfo.isSymLink())) {
        m_errorMessage = tr("<p>„%1“ ist weder eine Datei, noch ein symbolischer Link.</p>").arg(
                            QDir::toNativeSeparators(dbFile));
        return Error::CouldNotOpen;
    }
    if (! fileInfo.isReadable()) {
        m_errorMessage = tr("<p>Die Datei „%1“ ist nicht lesbar. Bitte die Zugriffsrechte "
                            "überprüfen!</p>").arg(QDir::toNativeSeparators(dbFile));
        return Error::CouldNotOpen;
    }

    // Try to establish a connection to the file
    if (! connectDatabase(dbFile)) {
        m_errorMessage = tr("<p>Es konnte keine Datenbankverbindung zu der Datei „%1“ aufgebaut "
                            "werden.</p>").arg(QDir::toNativeSeparators(dbFile));
        return Error::CouldNotOpen;
    }

    // We could connect to the file

    // Let's see if we can acquire a lock (before starting a normal transaction)
    if (checkIfDbIsLocked()) {
        m_errorMessage = tr(
            "<p><b>Unerwarteter Fehler:</b> Die Datenbank ist gesperrt</p>"
            "<p>Liegt die Datenbank auf einer Windows-Netzwerk-Freigabe (bzw. einem "
            "Samba-Share/CIFS-Mount)? Das Öffnen einer solchen Datenbank ist nur mit "
            "dateisystemseitigem Schreibschutz möglich, wir brauchen aber Schreibrechte.</p>"
            "<p>Ansonsten sollte das nicht passieren, sofern kein anderer Prozess die Datenbank "
            "gesperrt hat (und das sollte eigentlich auch nicht der Fall sein)!");
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // We have to make sure we're the only instance using it
    if (! lockDatabase()) {
        m_errorMessage = tr(
            "<p>Die Datenbank ist bereits von einer anderen Instanz vom Flohmarkt-WWS geöffnet.</p>"
            "<p>Sollte dies nicht der Fall sein (evtl. nach einen Crash, Stromausfall etc.), dann "
            "bitte die Lock-Datei „%1“ manuell löschen und die Datenbank erneut öffnen.</p>").arg(
            QDir::toNativeSeparators(lockFileName(dbFile)));
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // First we try if we can start a transaction (needed for the write test later) ...
    if (! m_db.transaction()) {
        m_errorMessage = tr(
            "<p>Für die Datenbank „%1“ konnte keine Transaktion gestartet werden. Das sollte nicht "
            "passieren. Die Datenbank kann nicht verwendet werden!</p>").arg(
            QDir::toNativeSeparators(dbFile));
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // ... then we create a query
    QSqlQuery query(m_db);

    // Do an internal check to see if it's an SQLite database and if it's in a sane condition
    try {
        queryExec(query, QStringLiteral("PRAGMA quick_check"));
        if (! query.next() || query.value(0).toString() != QLatin1String("ok")) {
            throw false;
        }
    } catch (...) {
        m_errorMessage = tr("<p>Der Datenbankcheck für die Datei „%1“ ist fehlgeschlagen.</p>").arg(
                            QDir::toNativeSeparators(dbFile))
                         + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Check if we actually have a Flohmarkt-WWS database

    // Check the db identifier

    try {
        queryPrepare(query, QStringLiteral("SELECT value FROM config WHERE key = ?"));
        query.bindValue(0, s_dbIdKey);
        queryExec(query);
        if (! query.next()) {
            m_errorMessage = tr(
                "<p>Die Datenbank-ID ist nicht auffindbar. Entweder ist die Datenbank „%1“ keine "
                "Flohmarkt-WWS-Datenbank, oder sie ist beschädigt, oder sie wurde unsachgemäß "
                "manuell verändert.</p>").arg(QDir::toNativeSeparators(dbFile));
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }

        if (query.value(0).toString() != s_dbId) {
            m_errorMessage = tr("<p>Die Datenbank „%1“ hat keine gültige Datenbank-ID. Vermutlich "
                                "ist es keine Flohmarkt-WWS-Datenbank.</p>").arg(
                                QDir::toNativeSeparators(dbFile));
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }

    } catch (...) {
        m_errorMessage = tr(
            "<p>Die Datenbank-ID konnte nicht ausgelesen werden. Entweder ist die Datenbank „%1“ "
            "keine Flohmarkt-WWS-Datenbank, oder sie ist beschädigt, oder sie wurde unsachgemäß "
            "manuell verändert.</p>").arg(QDir::toNativeSeparators(dbFile))
            + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Let's see if we have a valid database revision config entry

    try {
        queryPrepare(query, QStringLiteral("SELECT value FROM config WHERE key = ?"));
        query.bindValue(0, s_dbVersionKey);
        queryExec(query);
        if (! query.next()) {
            m_errorMessage = tr(
                "<p>Die verwendete Datenbankrevision konnte nicht ausgelesen werden.</p>"
                "<p>Entweder ist die Datenbank „%1“ beschädigt, oder sie wurde unsachgemäß manuell "
                "verändert.</p>").arg(QDir::toNativeSeparators(dbFile));
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }

        if (query.value(0).toInt() != s_dbVersion) {
            m_errorMessage = tr(
                "<p>Die verwendete Datenbankrevision Ist unbekannt.</p>"
                "<p>Entweder ist die Datenbank „%1“ von einer neueren Version vom Flohmarkt-WWS "
                "erstellt wordem, oder sie wurde unsachgemäß manuell verändert.</p>").arg(
                QDir::toNativeSeparators(dbFile));
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }

    } catch (...) {
        m_errorMessage = tr(
            "<p>Die verwendete Datenbankrevision konnte nicht ausgelesen werden.</p>"
            "<p>Entweder ist die Datenbank „%1“ beschädigt, oder sie wurde unsachgemäß manuell "
            " verändert.</p>").arg(QDir::toNativeSeparators(dbFile))
            + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Read the commission value
    try {
        queryPrepare(query, QStringLiteral("SELECT value FROM config WHERE key = ?"));
        query.bindValue(0, s_commissionKey);
        queryExec(query);
        if (! query.next()) {
            m_errorMessage = tr(
                "<p>Die Verkaufsprovision konnte nicht ausgelesen werden.</p>"
                "<p>Entweder ist die Datenbank „%1“ beschädigt, oder sie wurde unsachgemäß manuell "
                "verändert.</p>").arg(QDir::toNativeSeparators(dbFile));
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }
        m_commission = query.value(0).toInt();
    } catch (...) {
        m_errorMessage = tr(
            "<p>Die Verkaufsprovision konnte nicht ausgelesen werden.</p>"
            "<p>Entweder ist die Datenbank „%1“ beschädigt, oder sie wurde unsachgemäß manuell "
            " verändert.</p>").arg(QDir::toNativeSeparators(dbFile))
            + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Read the name value
    try {
        queryPrepare(query, QStringLiteral("SELECT value FROM config WHERE key = ?"));
        query.bindValue(0, s_eventNameKey);
        queryExec(query);
        if (! query.next()) {
            m_errorMessage = tr(
                "<p>Der Name der Veranstaltung konnte nicht ausgelesen werden.</p>"
                "<p>Entweder ist die Datenbank „%1“ beschädigt, oder sie wurde unsachgemäß manuell "
                "verändert.</p>").arg(QDir::toNativeSeparators(dbFile));
            closeDatabaseConnection();
            return Error::CouldNotOpen;
        }
        m_eventName = query.value(0).toString();
    } catch (...) {
        m_errorMessage = tr(
            "<p>Der Name der Veranstaltung konnte nicht ausgelesen werden.</p>"
            "<p>Entweder ist die Datenbank „%1“ beschädigt, oder sie wurde unsachgemäß manuell "
            " verändert.</p>").arg(QDir::toNativeSeparators(dbFile))
            + sqlErrorText(query);
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Let's see if we can write to the database.
    //
    // We do have to check if a database is writable by actually trying to insert some data (is
    // this a bug or a feature?!) because:
    //
    // - If the file itself has write access, QFileInfo::isWritable() will return true, but if the
    //   parent directory is not writable, we won't be able to write to the database.
    //
    // - Checking for the parent directory to be writable in addition does also not help, as we will
    //   get false for "My Documents" on Windows, although we can write to it.
    //
    // - Opening the file via QFile::open() does actually work if the file is writable and the
    //   parent directory is not (with the QIODevice::WriteOnly flag, the file will even be
    //   trucated!), but we still can't insert data in this case.
    //
    // So here we go:

    bool isWritable = false;
    try {
        // Even if the database is locked by another instance, it's save to do this here:
        // The transaction won't be actually written to the database. And even if it was,
        // it wouldn't affect the other instance, because s_keyWriteTest is never used.
        // So this is save in each case:
        queryPrepare(query, QStringLiteral("INSERT INTO config(key, value) VALUES(?, ?)"));
        query.bindValue(0, s_writeTestKey);
        query.bindValue(1, QStringLiteral("test"));
        queryExec(query);
        isWritable = true;
    } catch (...) {
        isWritable = false;
    }

    // Here, we don't interact with the database anymore, so we can rollback the transaction
    // so that no changes will be actually written to the database
    rollbackTransaction(query);

    if (! isWritable) {
        m_errorMessage = tr(
            "<p>Die Datenbank „%1“ kann SQL-seitig nicht geschrieben werden. Das sollte eigentlich "
            "nicht passieren. Bitte die Zugriffsrechte überprüfen, bzw. ob andere Prozesse auf die "
            "Datenbank zugreifen!</p>").arg(QDir::toNativeSeparators(dbFile));
        closeDatabaseConnection();
        return Error::CouldNotOpen;
    }

    // Everything is fine :-)

    // From here on, not a single SQL query should fail, so we activate the MainWindow's SQL
    // error handling (which displays a critical error and closes the database if something goes
    // wrong)
    m_emitSqlErrors = true;
    Q_EMIT databaseChanged();
    return Error::NoError;
}

const QString &Database::errorMessage() const
{
    return m_errorMessage;
}

const QString &Database::dbFile() const
{
    return m_dbFileName;
}

const QString &Database::dbPath() const
{
    return m_dbFilePath;
}

bool Database::connectDatabase(const QString &dbFile)
{
    // First close a connection if we have one
    closeDatabaseConnection();

    // Connect the database
    m_db = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), s_connectionName);
    m_db.setDatabaseName(dbFile);

    updateDbFileInfo();

    if (! m_db.open()) {
        // This should not happen
        QSqlError error = m_db.lastError();
        m_errorMessage = tr("<p>Unerwarteter Fehler: Konnte keine Datenbankverbindung herstellen."
                            "</p>"
                            "<p>Die Fehlermeldung war: %2</p>").arg(error.text());
        return false;
    }

    return true;
}

void Database::closeDatabaseConnection()
{
    if (! m_db.isOpen()) {
        return;
    }

    // Close the existing connection
    m_db.close();
    m_db = QSqlDatabase::database();
    QSqlDatabase::removeDatabase(s_connectionName);

    // Remove the lock file
    if (m_lockFile != nullptr) {
        delete m_lockFile;
        m_lockFile = nullptr;
    }

    // Don't emit SQL error signals
    m_emitSqlErrors = false;
}

bool Database::checkIfDbIsLocked()
{
    QSqlQuery query(m_db);
    if (query.exec(QStringLiteral("BEGIN EXCLUSIVE"))) {
        query.exec(QStringLiteral("COMMIT"));
        return false;
    } else {
        return true;
    }
}

bool Database::lockDatabase()
{
    m_lockFile = new QLockFile(lockFileName(m_dbFileName));
    m_lockFile->setStaleLockTime(0);

    if (! m_lockFile->tryLock(0)) {
        delete m_lockFile;
        m_lockFile = nullptr;
        return false;
    }

    return true;
}

QString Database::checkForLockFile(const QString &dbFile) const
{
    // If the file doesn't exist, it can't be locked
    QFileInfo fileInfo(dbFile);
    if (! fileInfo.exists()) {
        return QString();
    }

    // If it exists, try to lock it
    const QString fileName(lockFileName(dbFile));
    QLockFile lockFile(fileName);
    lockFile.setStaleLockTime(0);
    const bool noLock = lockFile.tryLock(0);
    return noLock ? QString() : fileName;
}

QString Database::lockFileName(const QString &dbFile) const
{
    const QFileInfo info(dbFile);
    QString path = info.path();
    if (path == QStringLiteral("/")) {
        path.clear();
    }
    return QStringLiteral("%1/.flohmarkt-wws.%2.lockfile").arg(path, info.fileName());
}

void Database::updateDbFileInfo()
{
    m_dbFileName = m_db.databaseName();
    const QFileInfo info(m_dbFileName);
    m_dbFilePath = info.canonicalPath();
}

bool Database::startTransaction()
{
    const bool success = m_db.transaction();
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br>QSqlDatabase::transaction</p>")
                     + sqlErrorText());
        throw false;
    }
    return success;
}

bool Database::commitTransaction()
{
    const bool success = m_db.commit();
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br>QSqlDatabase::commit</p>")
                     + sqlErrorText());
        throw false;
    }
    return success;
}

bool Database::rollbackTransaction(QSqlQuery &query)
{
    query.clear();
    return m_db.rollback();
}

QString Database::sqlErrorText() const
{
    const QSqlError error = m_db.lastError();
    return tr("<p><b>Die Fehlermeldung war:</b><br>%1<br>%2</p>").arg(
              error.driverText(), error.databaseText());
}

QString Database::sqlErrorText(const QSqlQuery &query) const
{
    const QSqlError error = query.lastError();
    QString message = tr("<p><b>Die Fehlermeldung war:</b><br>%1<br>%2</p>").arg(
                         error.driverText(), error.databaseText());
    const QString executedQuery = query.executedQuery();
    if (! executedQuery.isEmpty()) {
        message.append(tr("<p><b>Die ausgeführte Datenbankabfrage war:</b><br>%3</p>").arg(
                          executedQuery));
    }
    return message;
}

void Database::emitSqlError(const QString &text)
{
    if (! m_emitSqlErrors) {
        return;
    }
    Q_EMIT sqlError(text);
}

bool Database::queryPrepare(QSqlQuery &query, const QString &text)
{
    const bool success = query.prepare(text);
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br>QSqlQuery::prepare</p>")
                     + sqlErrorText(query));
        throw false;
    }
    return success;
}

bool Database::queryExec(QSqlQuery &query)
{
    const bool success = query.exec();
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br>QSqlQuery::exec</p>")
                     + sqlErrorText(query));
        throw false;
    }
    return success;
}

bool Database::queryExec(QSqlQuery &query, const QString &text)
{
    const bool success = query.exec(text);
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br>QSqlQuery::exec</p>")
                     + sqlErrorText(query));
        throw false;
    }
    return success;
}

bool Database::queryExecBatch(QSqlQuery &query)
{
    const bool success = query.execBatch();
    if (! success) {
        emitSqlError(tr("<p><b>Fehlgeschlagene Funktion:</b><br>QSqlQuery::execBatch</p>")
                     + sqlErrorText(query));
        throw false;
    }
    return success;
}

// ================================================================================================
//  Functions called externally
// ================================================================================================

const QString &Database::eventName() const
{
    return m_eventName;
}

bool Database::setEventName(const QString &name)
{
    const auto success = writeHelper([this, name](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("UPDATE config SET value = ? WHERE key = ?"));
        query.bindValue(0, name);
        query.bindValue(1, s_eventNameKey);
        queryExec(query);
    });

    if (! success) {
        return false;
    }
    m_eventName = name;
    return true;
}

int Database::commission() const
{
    return m_commission;
}

bool Database::setCommission(int value)
{
    const auto success = writeHelper([this, value](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("UPDATE config SET value = ? WHERE key = ?"));
        query.bindValue(0, value);
        query.bindValue(1, s_commissionKey);
        queryExec(query);
    });

    if (! success) {
        return false;
    }
    m_commission = value;
    Q_EMIT commissionChanged();
    return true;
}

Database::TSuccess<int> Database::soldItems()
{
    int result = 0;
    bool success = false;

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT SUM(sold) FROM items"));
        if (query.next()) {
            result = query.value(0).toInt();
        }
    }, success);

    return { result, success };
}

Database::TSuccess<bool> Database::vendorRegistered(const QString &id)
{
    bool result = false;
    bool success = false;

    readHelper([this, &id, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("SELECT id FROM vendors WHERE id = ?"));
        query.bindValue(0, id);
        queryExec(query);

        if (query.next()) {
            result = true;
        }
    }, success);

    return { result, success };
}

bool Database::registerVendor(Flohmarkt::VendorData data)
{
    bool nameChanged = false;
    bool listNumberChanged = false;

    bool success1;
    QVector<VendorData> vendors;

    readHelper([this, &vendors](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT id, name, listNumber FROM vendors"));
        while (query.next()) {
            vendors.append({ query.value(0).toString(),
                             query.value(1).toString(),
                             query.value(2).toString() });
        }
    }, success1);
    if (! success1) {
        return false;
    }

    // Search for exact collisions in the name

    qCDebug(FlohmarktLog) << "Searching for exact name collisions for" << data.name;
    bool exactNameCollisionFound = false;

    for (const auto &vendor : std::as_const(vendors)) {
        if (vendor.name == data.name) {
            qCDebug(FlohmarktLog) << "Found an exact name collision, appending (1) and (2)";
            exactNameCollisionFound = true;
            const auto newName = QStringLiteral("%1 (1)").arg(vendor.name);

            // Append a consecutive number to the existing vendor name
            const auto success2 = writeHelper([this, &vendor, newName](QSqlQuery &query) {
                queryPrepare(query, QStringLiteral("UPDATE vendors SET name = ? WHERE id = ?"));
                query.bindValue(0, newName);
                query.bindValue(1, vendor.id);
                queryExec(query);
            });
            if (! success2) {
                return false;
            }

            // Inform the item lists page
            Q_EMIT vendorRenamed(vendor.id, newName);

            // ... and to the one to be registered
            data.name.append(QLatin1String(" (2)"));
            nameChanged = true;
        }
    }

    // Search for exact collisions in the list number

    qCDebug(FlohmarktLog) << "Searching for exact list number collisions for" << data.listNumber;
    bool exactListNumberCollisionFound = false;

    for (const auto &vendor : std::as_const(vendors)) {
        if (vendor.listNumber == data.listNumber) {
            qCDebug(FlohmarktLog) << "Found an exact list number collision, appending (1) and (2)";
            exactListNumberCollisionFound = true;
            const auto newListNumber = QStringLiteral("%1 (1)").arg(vendor.listNumber);

            // Append a consecutive number to the existing vendor name
            const auto success3 = writeHelper([this, &vendor, newListNumber](QSqlQuery &query) {
                queryPrepare(query, QStringLiteral("UPDATE vendors SET listNumber = ? "
                                                   "WHERE id = ?"));
                query.bindValue(0, newListNumber);
                query.bindValue(1, vendor.id);
                queryExec(query);
            });
            if (! success3) {
                return false;
            }

            // Inform the item lists page
            Q_EMIT listNumberRenamed(vendor.id, newListNumber);

            // ... and to the one to be registered
            data.listNumber.append(QLatin1String(" (2)"));
            listNumberChanged = true;
        }
    }

    // Search for existing, already renamed vendor names

    if (! exactNameCollisionFound) {
        qCDebug(FlohmarktLog) << "Searching for numbered name collisions for" << data.name;
        bool matchFound = false;
        for (const auto &vendor : std::as_const(vendors)) {
            if (vendor.name.back() != (QLatin1Char(')'))) {
                // We can cheap out the costly regular expression match in this case
                continue;
            }
            const auto match = s_consecutiveNumber.match(vendor.name);
            if (match.hasMatch() && match.captured(1) == data.name) {
                matchFound = true;
                break;
            }
        }

        if (matchFound) {
            qCDebug(FlohmarktLog) << "Found a numbered name collision. Searching for next free "
                                     "number";
            QVector<QString> names;
            for (const auto &vendor : std::as_const(vendors)) {
                names.append(vendor.name);
            }

            QString newName;
            int number = 3; // (1) and (2) are already handled by the above routines
            while (true) {
                newName = QStringLiteral("%1 (%2)").arg(data.name, QString::number(number));
                if (! names.contains(newName)) {
                    break;
                }
                number++;
            }

            qCDebug(FlohmarktLog) << "Next free number is" << number << "- appending it";
            data.name = newName;
            nameChanged = true;
        }
    }

    // Search for existing, already renamed list numbers

    if (! exactListNumberCollisionFound) {
        qCDebug(FlohmarktLog) << "Searching for numbered list number collisions for"
                              << data.listNumber;
        bool matchFound = false;
        for (const auto &vendor : std::as_const(vendors)) {
            if (vendor.listNumber.back() != (QLatin1Char(')'))) {
                // We can cheap out the costly regular expression match in this case
                continue;
            }
            const auto match = s_consecutiveNumber.match(vendor.listNumber);
            if (match.hasMatch() && match.captured(1) == data.listNumber) {
                matchFound = true;
                break;
            }
        }

        if (matchFound) {
            qCDebug(FlohmarktLog) << "Found a numbered list number collision. Searching for next "
                                     "free number";
            QVector<QString> listNumbers;
            for (const auto &vendor : std::as_const(vendors)) {
                listNumbers.append(vendor.listNumber);
            }

            QString newListNumber;
            int number = 3; // (1) and (2) are already handled by the above routines
            while (true) {
                newListNumber = QStringLiteral("%1 (%2)").arg(data.listNumber,
                                                              QString::number(number));
                if (! listNumbers.contains(newListNumber)) {
                    break;
                }
                number++;
            }

            qCDebug(FlohmarktLog) << "Next free number is" << number << "- appending it";
            data.listNumber = newListNumber;
            listNumberChanged = true;
        }
    }

    // Actual data insertion

    const auto success4 = writeHelper([this, &data](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("INSERT INTO vendors(id, name, listNumber) "
                                           "VALUES(?, ?, ?)"));
        query.bindValue(0, data.id);
        query.bindValue(1, data.name);
        query.bindValue(2, data.listNumber);
        queryExec(query);
    });

    if (! success4) {
        return false;
    }

    qCDebug(FlohmarktLog) << "Added new vendor:" << "\n"
                          << "    ID         :" << data.id << "\n"
                          << "    Name       :" << data.name << "\n"
                          << "    List number:" << data.listNumber;

    Q_EMIT vendorChanged(data, VendorChange::VendorRegistered);

    if (nameChanged || listNumberChanged) {
        Q_EMIT datasetAltered(data.name, data.listNumber);
    }

    return true;
}

bool Database::deleteVendor(const Flohmarkt::VendorData &data)
{
    const auto success = writeHelper([this, &data](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("DELETE FROM vendors WHERE id = ?"));
        query.bindValue(0, data.id);
        queryExec(query);
        queryPrepare(query, QStringLiteral("DELETE FROM items WHERE vendor = ?"));
        query.bindValue(0, data.id);
        queryExec(query);
    });

    if (! success) {
        return false;
    }
    Q_EMIT vendorChanged(data, VendorChange::VendorDeleted);
    return true;
}

Database::TSuccess<Database::ItemStatus> Database::itemStatus(const QString &id)
{
    ItemStatus result = ItemStatus::ItemNotPresent;
    bool success = false;

    readHelper([this, &id, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("SELECT sold FROM items WHERE id = ?"));
        query.bindValue(0, id);
        queryExec(query);
        if (query.next()) {
            result = query.value(0).toInt() == 0 ? ItemStatus::ItemAvailable : ItemStatus::ItemSold;
        }
    }, success);

    return { result, success };
}

Database::TSuccess<Database::ItemVendorData> Database::itemVendorData(const QString &vendorId)
{
    ItemVendorData result;
    bool success = false;

    readHelper([this, &vendorId, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("SELECT name, listNumber FROM vendors WHERE id = ?"));
        query.bindValue(0, vendorId);
        queryExec(query);
        if (query.next()) {
            result.name = query.value(0).toString();
            result.listNumber = query.value(1).toString();
        }
    }, success);

    return { result, success };
}

bool Database::registerItem(const Flohmarkt::ItemData &data)
{
    const auto success = writeHelper([this, &data](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("INSERT INTO items(id, "
                                                             "vendor, "
                                                             "listNumber, "
                                                             "description, "
                                                             "size, "
                                                             "price, "
                                                             "sold) "
                                           "VALUES(?, ?, ?, ?, ?, ?, ?)"));
        query.bindValue(0, data.itemId);
        query.bindValue(1, data.vendorId);
        query.bindValue(2, data.listNumber);
        query.bindValue(3, data.description);
        query.bindValue(4, data.size);
        query.bindValue(5, data.price);
        query.bindValue(6, 0);
        queryExec(query);
    });

    if (! success) {
        return false;
    }
    Q_EMIT itemChanged(data, ItemChange::ItemRegistered);
    return true;
}

bool Database::deleteItem(const Flohmarkt::ItemData &data)
{
    const auto success = writeHelper([this, &data](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("DELETE FROM items WHERE id = ?"));
        query.bindValue(0, data.itemId);
        queryExec(query);
    });

    if (! success) {
        return false;
    }
    Q_EMIT itemChanged(data, ItemChange::ItemDeleted);
    return true;
}

bool Database::markItemAsSold(const Flohmarkt::ItemData &data, bool sold)
{
    const auto success = writeHelper([this, &data, sold](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("UPDATE items SET sold = ? WHERE id = ?"));
        query.bindValue(0, sold ? 1 : 0);
        query.bindValue(1, data.itemId);
        queryExec(query);
    });

    if (! success) {
        return false;
    }
    Q_EMIT itemChanged(data, sold ? ItemChange::ItemMarkedAsSold : ItemChange::ItemMarkedAsNotSold);
    return true;
}

bool Database::enterSoldItems(const QVector<Flohmarkt::ItemData> &data)
{
    Q_ASSERT(data.count() > 0);

    const auto success = writeHelper([this, &data](QSqlQuery &query) {
        const QVector placeholders(data.count(), QStringLiteral("?"));
        queryPrepare(query, QStringLiteral(
            "UPDATE items SET sold = 1 WHERE id IN (%1)").arg(
            QStringList::fromVector(placeholders).join(QStringLiteral(", "))));

        for (int i = 0; i < data.count(); i++) {
            query.bindValue(i, data.at(i).itemId);
        }

        queryExec(query);
    });

    if (! success) {
        return false;
    }
    Q_EMIT soldItemsEntered(data);
    return true;
}

Database::TSuccess<QVector<Flohmarkt::VendorData>> Database::vendors()
{
    QVector<Flohmarkt::VendorData> result;
    bool success = false;

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT id, name, listNumber FROM vendors"));
        while (query.next()) {
            result.append(Flohmarkt::VendorData {
                query.value(0).toString(),
                query.value(1).toString(),
                query.value(2).toString(),
            });
        }
    }, success);

    return { result, success };
}

Database::TSuccess<QVector<Database::ItemListData>> Database::itemsForVendor(const QString &id)
{
    QVector<ItemListData> result;
    bool success = false;

    readHelper([this, &id, &result](QSqlQuery &query) {
        queryPrepare(query, QStringLiteral("SELECT id, listNumber, description, size, price, sold "
                                           "FROM items WHERE vendor = ? "
                                           "ORDER BY listNumber"));
        query.bindValue(0, id);
        queryExec(query);
        while (query.next()) {
            result.append(ItemListData {
                query.value(0).toString(),
                query.value(1).toInt(),
                query.value(2).toString(),
                query.value(3).toString(),
                query.value(4).toInt(),
                (query.value(5).toInt() == 1),
            });
        }
    }, success);

    return { result, success };
}

Database::TSuccess<Database::Overview> Database::overview()
{
    Overview result;
    bool success = false;

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT COUNT(id) FROM vendors"));
        if (query.next()) {
            result.vendors = query.value(0).toInt();
        }
    }, success);

    if (! success) {
        return { result, false };
    }

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT COUNT(id) FROM items"));
        if (query.next()) {
            result.items = query.value(0).toInt();
        }
    }, success);

    if (! success) {
        return { result, false };
    }

    readHelper([this, &result](QSqlQuery &query) {
        queryExec(query, QStringLiteral("SELECT SUM(sold), SUM(price) FROM items "
                                        "WHERE sold = 1"));
        if (query.next()) {
            result.sold = query.value(0).toInt();
            result.sales = query.value(1).toInt();
        }
    }, success);

    return { result, success };
}
