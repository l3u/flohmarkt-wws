// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SHOPPINGLISTPAGE_H
#define SHOPPINGLISTPAGE_H

// Local includes
#include "AbstractListPage.h"
#include "Database.h"
#include "shared/Flohmarkt.h"

// Local classes
class ScanWidget;
class ShoppingListModel;

// Qt classes
class QTableView;
class QLabel;
class QMenu;
class QPushButton;
class QFile;

class ShoppingListPage : public AbstractListPage
{
    Q_OBJECT

public:
    explicit ShoppingListPage(Database *database, QWidget *parent = nullptr);
    void startScan();

public Q_SLOTS:
    void scannedItemData(const Flohmarkt::ItemData &data);
    void scannedVendorData();

private Q_SLOTS:
    void showContextMenu(QPoint point);
    void removeItem();
    void commissionChanged();
    void updateSum();
    void itemChanged(const Flohmarkt::ItemData &data, Database::ItemChange change);
    void vendorChanged(const Flohmarkt::VendorData &data, Database::VendorChange change);
    void enterShoppingList();
    void saveShoppingList();
    void printShoppingList();

private: // Functions
    void generateHtml(QFile &file);

private: // Variables
    Database *m_database;
    ScanWidget *m_scanWidget;
    ShoppingListModel *m_model;

    QMenu *m_contextMenu;
    int m_selectedRow = -1;

    QTableView *m_view;
    QLabel *m_sum;
    QLabel *m_commissionLabel;
    QLabel *m_commission;

    QPushButton *m_print;
    QPushButton *m_save;
    QPushButton *m_enter;

};

#endif // SHOPPINGLISTPAGE_H
