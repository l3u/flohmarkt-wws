// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ITEMSLISTMODEL_H
#define ITEMSLISTMODEL_H

// Local includes
#include "Database.h"

// Qt includes
#include <QAbstractTableModel>
#include <QLocale>
#include <QVector>

class ItemsListModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ItemsListModel(Database *database, QObject *parent = nullptr);
    int rowCount(const QModelIndex &) const override;
    int columnCount(const QModelIndex &) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &) const override;

    void setVendorId(const QString &vendorId);
    void refresh();
    void clear();

    int soldItems() const;
    int salesVolume() const;
    const Database::ItemListData &itemData(int row) const;
    const QVector<Database::ItemListData> &items() const;

private: // Variables
    Database *m_database;
    const QLocale m_locale;
    QVector<Database::ItemListData> m_items;
    QString m_vendorId;
    int m_soldItems = 0;
    int m_salesVolume = 0;

};

#endif // ITEMSLISTMODEL_H
