// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ScanWidget.h"
#include "ScanLineEdit.h"
#include "shared/IconButton.h"
#include "shared/UuidHelper.h"
#include "shared/QrCodeGenerator.h"
#include "shared/Logging.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QJsonParseError>
#include <QJsonObject>
#include <QTimer>

ScanWidget::ScanWidget(QWidget *parent) : QWidget(parent)
{
    auto *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto *box = new QGroupBox(tr("QR-Code scannen"));
    box->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Maximum);
    auto *boxLayout = new QHBoxLayout(box);

    auto *scanButton = new IconButton(tr("Scannen"), QStringLiteral("qr-code"));
    connect(scanButton, &QPushButton::clicked, this, &ScanWidget::startScan);
    boxLayout->addWidget(scanButton);

    m_data = new ScanLineEdit;
    connect(m_data, &ScanLineEdit::focusIn, this, &ScanWidget::displayState);
    connect(m_data, &QLineEdit::returnPressed, this, &ScanWidget::parseData);
    boxLayout->addWidget(m_data);

    m_state = new QLabel;
    boxLayout->addWidget(m_state);
    displayState(false);

    layout->addWidget(box);
}

void ScanWidget::startScan()
{
    QTimer::singleShot(0, m_data, QOverload<>::of(&QLineEdit::setFocus));
}

void ScanWidget::displayState(bool ready)
{
    m_state->setText(ready ? tr("<p style=\"color: green;\"><b>Bereit zum Scannen</b></p>")
                           : tr("<p style=\"color: red;\"><b>Nicht bereit zum Scannen</b></p>"));
}

void ScanWidget::parseData()
{
    const auto data = m_data->text().toUtf8();
    m_data->clear();

    const auto possibleExplanation = tr(
        "<p>Entweder ist beim Scannen etwas schiefgegangen, oder der Code ist kein "
        "Flohmarkt-WWS-QR-Code, oder er ist beschädigt.</p>");

    if (data.count() < 8) {
        QMessageBox::warning(this,
            tr("QR-Code scannen"),
            tr("<p>Der QR-Code konnte nicht verarbeitet werden! Er enthält keine kompletten "
               "Headerdaten.</p>")
            + possibleExplanation);
        Q_EMIT scannedInvalidData();
        return;
    }

    // Code ID header (4 chars)
    const auto codeId = QString::fromUtf8(data.mid(0, 4));
    if (codeId != Flohmarkt::Qr::codeId) {
        QMessageBox::warning(this,
            tr("QR-Code scannen"),
            tr("<p>Der QR-Code konnte nicht verarbeitet werden! Er enthält keinen bekannten "
               "Header.</p>")
            + possibleExplanation);
        Q_EMIT scannedInvalidData();
        return;
    }

    // Protocol version (2 chars that represent the hex numer as ASCII characters)
    const auto hexProtocolVersion = QString::fromUtf8(data.mid(4, 2));
    bool convertedHex = false;
    const auto protocolVersion = hexProtocolVersion.toUInt(&convertedHex, 16);
    if (! convertedHex) {
        QMessageBox::warning(this,
            tr("QR-Code scannen"),
            tr("<p>Der QR-Code konnte nicht verarbeitet werden! Die Protokollversion konnte "
               "nicht ausgelesen werden.</p>")
            + possibleExplanation);
        Q_EMIT scannedInvalidData();
        return;
    }
    if (protocolVersion != Flohmarkt::Qr::protocolVersion) {
        QMessageBox::warning(this,
            tr("QR-Code scannen"),
            tr("<p>Der QR-Code konnte nicht verarbeitet werden! Die Protokollversion ist nicht "
               "bekannt.</p>"
               "<p>Evtl. wurde der Code mit einer neueren Version des Programms erstellt? "
               "Wenn nicht, dann:</p>")
            + possibleExplanation);
        Q_EMIT scannedInvalidData();
        return;
    }

    // Code type (1 char, either "v" for "vendor code" or "i" for "item code")
    const auto codeType = QString::fromUtf8(data.mid(6, 1));
    if (! Flohmarkt::Qr::stringToType.contains(codeType)) {
        QMessageBox::warning(this,
            tr("QR-Code scannen"),
            tr("<p>Der QR-Code konnte nicht verarbeitet werden! Der Code-Typ konnte nicht "
               "ausgelesen werden</p>")
            + possibleExplanation);
        Q_EMIT scannedInvalidData();
        return;
    }
    const auto type = Flohmarkt::Qr::stringToType.value(codeType);

    // The JSON representation of the actual data
    const auto jsonData = data.mid(7, -1);
    QJsonParseError parseError;
    const auto jsonDocument = QJsonDocument::fromJson(jsonData, &parseError);
    if (parseError.error != QJsonParseError::NoError){
        QMessageBox::warning(this,
            tr("QR-Code scannen"),
            tr("<p>Der QR-Code konnte nicht geparst werden! Die Fehlermeldung war:</p>"
               "<p>Fehler bei Offset %1: %2</p>").arg(
               QString::number(parseError.offset), parseError.errorString())
            + possibleExplanation);
        Q_EMIT scannedInvalidData();
        return;
    }

    const auto jsonObject = jsonDocument.object();
    const auto vendorId = jsonObject.value(Flohmarkt::Qr::vendorId).toString();
    // This will silently fail and return an empty string for a vendor Qr code without an item ID
    const auto itemId = jsonObject.value(Flohmarkt::Qr::itemId).toString();

    switch (type) {
    case Flohmarkt::Qr::ItemCode:
        if (! UuidHelper::checkUuid(itemId)) {
            QMessageBox::warning(this,
                tr("QR-Code scannen"),
                tr("<p>Der QR-Code konnte nicht verarbeitet werden! Die Artikel-ID ist ungültig."
                   "</p>")
                + possibleExplanation);
            Q_EMIT scannedInvalidData();
            return;
        }
        [[fallthrough]];
    default:
        if (! UuidHelper::checkUuid(vendorId)) {
            QMessageBox::warning(this,
                tr("QR-Code scannen"),
                tr("<p>Der QR-Code konnte nicht verarbeitet werden! Die Verkäufer-ID ist ungültig."
                   "</p>")
                + possibleExplanation);
            Q_EMIT scannedInvalidData();
            return;
        }
        break;
    }

    // The code should be correct

    if (type == Flohmarkt::Qr::VendorCode) {
        const auto vendorName = jsonObject.value(Flohmarkt::Qr::vendorName);
        if (vendorName.isUndefined() || vendorName.type() != QJsonValue::String) {
            QMessageBox::warning(this,
                tr("QR-Code scannen"),
                tr("<p>Der QR-Code konnte nicht verarbeitet werden! Der Verkäufername fehlt oder "
                   "ist ungültig.</p>")
                + possibleExplanation);
            Q_EMIT scannedInvalidData();
            return;
        }

        const auto vendorListNumber = jsonObject.value(Flohmarkt::Qr::vendorListNumber);
        if (vendorListNumber.isUndefined() || vendorListNumber.type() != QJsonValue::String) {
            QMessageBox::warning(this,
                tr("QR-Code scannen"),
                tr("<p>Der QR-Code konnte nicht verarbeitet werden! Die Listennummer fehlt oder "
                   "ist ungültig.</p>")
                + possibleExplanation);
            Q_EMIT scannedInvalidData();
            return;
        }

        Q_EMIT scannedVendorData(Flohmarkt::VendorData {
            vendorId,
            QrCodeGenerator::unEscape(vendorName.toString()),
            vendorListNumber.toString()
        });

    } else if (type == Flohmarkt::Qr::ItemCode) {
        const auto itemNumber = jsonObject.value(Flohmarkt::Qr::itemNumber);
        if (itemNumber.isUndefined() || itemNumber.type() != QJsonValue::Double) {
            QMessageBox::warning(this,
                tr("QR-Code scannen"),
                tr("<p>Der QR-Code konnte nicht verarbeitet werden! Die Artikelnummer fehlt oder "
                   "ist ungültig.</p>")
                + possibleExplanation);
            Q_EMIT scannedInvalidData();
            return;
        }

        const auto itemDescription = jsonObject.value(Flohmarkt::Qr::itemDescription);
        if (itemDescription.isUndefined() || itemDescription.type() != QJsonValue::String) {
            QMessageBox::warning(this,
                tr("QR-Code scannen"),
                tr("<p>Der QR-Code konnte nicht verarbeitet werden! Die Artikelbeschreibung fehlt "
                   "oder ist ungültig.</p>")
                + possibleExplanation);
            Q_EMIT scannedInvalidData();
            return;
        }

        const auto itemSize = jsonObject.value(Flohmarkt::Qr::itemSize);
        if (itemSize.isUndefined() || itemSize.type() != QJsonValue::String) {
            QMessageBox::warning(this,
                tr("QR-Code scannen"),
                tr("<p>Der QR-Code konnte nicht verarbeitet werden! Die Artikelgröße fehlt "
                   "oder ist ungültig.</p>")
                + possibleExplanation);
            Q_EMIT scannedInvalidData();
            return;
        }

        const auto itemPrice = jsonObject.value(Flohmarkt::Qr::itemPrice);
        if (itemPrice.isUndefined() || itemPrice.type() != QJsonValue::Double) {
            QMessageBox::warning(this,
                tr("QR-Code scannen"),
                tr("<p>Der QR-Code konnte nicht verarbeitet werden! Der Artikelpreis fehlt oder "
                   "ist ungültig.</p>")
                + possibleExplanation);
            Q_EMIT scannedInvalidData();
            return;
        }

        Q_EMIT scannedItemData(Flohmarkt::ItemData {
            vendorId,
            itemId,
            itemNumber.toInt(),
            QrCodeGenerator::unEscape(itemDescription.toString()),
            QrCodeGenerator::unEscape(itemSize.toString()),
            itemPrice.toInt(),
        });

    } else {
        // This should not happen
        qCWarning(FlohmarktLog) << "Unhandled QR code type:" << type;
        Q_EMIT scannedInvalidData();
    }
}
