// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SCANWIDGET_H
#define SCANWIDGET_H

// Local includes
#include "shared/Flohmarkt.h"

// Qt includes
#include <QWidget>

// Qt classes
class QLabel;

// Local classes
class ScanLineEdit;

class ScanWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ScanWidget(QWidget *parent = nullptr);
    void startScan();

Q_SIGNALS:
    void scannedInvalidData();
    void scannedVendorData(const Flohmarkt::VendorData &data);
    void scannedItemData(const Flohmarkt::ItemData &data);

private Q_SLOTS:
    void displayState(bool ready);
    void parseData();

private: // Variables
    ScanLineEdit *m_data;
    QLabel *m_state;

};

#endif // SCANWIDGET_H
