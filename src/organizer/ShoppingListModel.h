// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SHOPPINGLISTMODEL_H
#define SHOPPINGLISTMODEL_H

// Local includes
#include "shared/Flohmarkt.h"

// Qt includes
#include <QAbstractTableModel>
#include <QLocale>

class ShoppingListModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ShoppingListModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &) const override;
    int columnCount(const QModelIndex &) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &) const override;

    bool itemPresent(const QString &id) const;
    void addItem(const Flohmarkt::ItemData &data);
    void removeItem(int row);
    void removeItem(const QString &id);
    int removeVendorItems(const QString &id);
    int sum() const;
    const QVector<Flohmarkt::ItemData> &items() const;
    void clear();

private: // Variables
    QLocale m_locale;
    QVector<Flohmarkt::ItemData> m_items;
    QVector<QString> m_ids;
    int m_sum = 0;

};

#endif // SHOPPINGLISTMODEL_H
