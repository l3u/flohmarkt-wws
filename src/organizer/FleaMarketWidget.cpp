// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "Database.h"
#include "BusyHelper.h"
#include "FleaMarketWidget.h"
#include "OverviewPage.h"
#include "ProcessDataPage.h"
#include "ShoppingListPage.h"
#include "ItemListsPage.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QTabWidget>
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>
#include <QTabBar>

FleaMarketWidget::FleaMarketWidget(QWidget *parent) : QWidget(parent)
{
    m_database = new Database(this);
    connect(m_database, &Database::sqlError, this, &FleaMarketWidget::sqlError);

    auto *layout = new QVBoxLayout(this);
    m_tabWidget = new QTabWidget;
    connect(m_tabWidget, &QTabWidget::currentChanged, this, &FleaMarketWidget::tabChanged);
    layout->addWidget(m_tabWidget);

    m_overviewPage = new OverviewPage(m_database);
    m_tabWidget->addTab(m_overviewPage, tr("Übersicht"));

    m_processDataPage = new ProcessDataPage(m_database);
    m_tabWidget->addTab(m_processDataPage, tr("Daten erfassen"));

    m_shoppingListPage = new ShoppingListPage(m_database);
    m_tabWidget->addTab(m_shoppingListPage, tr("Kassenzettel"));

    m_itemListsPage = new ItemListsPage(m_database);
    connect(m_itemListsPage, &ItemListsPage::requestProcessItem,
            this, &FleaMarketWidget::processItem);
    connect(m_itemListsPage, &ItemListsPage::requestProcessVendor,
            this, &FleaMarketWidget::processVendor);
    connect(m_itemListsPage, &ItemListsPage::requestAddToShoppingList,
            this, &FleaMarketWidget::addToShoppingList);
    m_tabWidget->addTab(m_itemListsPage, tr("Artikellisten"));
}

bool FleaMarketWidget::createDatabase()
{
    const auto fileName = QFileDialog::getSaveFileName(this,
        tr("Flohmarkt-Datenbank erstellen"),
        QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first().append(
            QLatin1String("/Flohmarkt.fwwsdb")),
        tr("Flohmarkt-Datenbanken (*.fwwsdb);;Alle Dateien (*)"));

    if (fileName.isEmpty()) {
        return false;
    }

    closeDatabase();

    // The user could have selected the currently open database
    if (fileName == m_database->dbFile()) {
        QMessageBox messageBox(this);
        messageBox.setIcon(QMessageBox::Warning);
        messageBox.setWindowTitle(tr("Flohmarkt-Datenbank erstellen"));
        messageBox.setText(tr(
            "Die ausgewählte Datei ist die momentan geöffnete Datenbank. Soll sie wirklich "
            "überschrieben werden? Alle Daten gehen dabei verloren!"));
        messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        messageBox.setDefaultButton(QMessageBox::Cancel);

        if (messageBox.exec() == QMessageBox::Cancel) {
            return false;
        }

        m_database->closeDatabaseConnection();
    }

    // The selected file could be a database opened by another instance
    const QString lockFile(m_database->checkForLockFile(fileName));
    if (! lockFile.isEmpty()) {
        QMessageBox::warning(this, tr("Flohmarkt-Datenbank erstellen"),
            tr("<p><b>Datei kann nicht überschrieben werden</b></p>"
               "<p>Die Datei „%1“ ist eine Datenbank, die momentan von einer anderen Instanz vom "
               "Flohmarkt-WWS geöffnet ist. Sie kann deswegen jetzt nicht überschrieben werden. "
               "Bitte erst die entsprechende Instanz bzw. die Datenbank schließen.</p>"
               "<p>Sollte dies nicht der Fall sein (evtl. nach einen Crash, Stromausfall etc.), "
               "dann bitte die Lock-Datei „%2“ manuell löschen und das Überschreiben erneut "
               "anfragen.<p>").arg(QDir::toNativeSeparators(fileName),
                                   QDir::toNativeSeparators(lockFile)));
        return false;
    }


    // Check if we can write the file. If it exists, it will be deleted, but the user has been
    // asked by the QFileDialog if this is okay. And yes, we have to do it in this way, because
    // QFileInfo::isWritable does not work as expected on Windows (returns false for
    // "My Documents").
    QFile test(fileName);
    if (test.open(QIODevice::WriteOnly)) {
        test.close();
        test.remove();
    } else {
        QMessageBox::warning(this, tr("Flohmarkt-Datenbank erstellen"),
            tr("Es bestehen keine Schreibrechte für die Datei „%1“. "
               "Bitte die Zugriffsrechte überprüfen!").arg(QDir::toNativeSeparators(fileName)));
        return false;
    }

    // The file should be okay, so here we go
    BusyHelper busyHelper(this, true);
    const auto success = m_database->createDatabase(fileName);
    if (! success) {
        busyHelper.reset();
        QMessageBox::warning(this, tr("Fehler beim Anlegen der Datenbank"),
                             m_database->errorMessage());
    }
    return success;
}

bool FleaMarketWidget::openDatabase()
{
    const auto fileName =  QFileDialog::getOpenFileName(this,
        tr("Flohmarkt-Datenbank öffnen"),
        QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first(),
        tr("Flohmarkt-Datenbanken (*.fwwsdb);;Alle Dateien (*)"));

    if (fileName.isEmpty()) {
        return false;
    }

    BusyHelper busyHelper(this, true);

    closeDatabase();

    const auto result = m_database->openDatabase(fileName);
    if (result != Database::NoError) {
        busyHelper.reset();
        QMessageBox::warning(this, tr("Fehler beim Öffnen der Datenbank"),
                             m_database->errorMessage());
        return false;

    } else {
        return true;
    }
}

void FleaMarketWidget::closeDatabase()
{
    m_database->closeDatabaseConnection();
    m_processDataPage->clear();
    m_itemListsPage->clear();
}

void FleaMarketWidget::processItem(const Flohmarkt::ItemData &data)
{
    m_tabWidget->setCurrentWidget(m_processDataPage);
    m_processDataPage->scannedItemData(data);
}

void FleaMarketWidget::processVendor(const Flohmarkt::VendorData &data)
{
    m_tabWidget->setCurrentWidget(m_processDataPage);
    m_processDataPage->scannedVendorData(data);
}

void FleaMarketWidget::addToShoppingList(const Flohmarkt::ItemData &data)
{
    m_shoppingListPage->scannedItemData(data);
    QMessageBox::information(this, tr("Artikel zu Kassenzettel hinzufügen"),
                             tr("Artikel „%1“ hinzugefügt!").arg(data.description));
}

void FleaMarketWidget::tabChanged(int index)
{
    if (index == m_tabWidget->indexOf(m_processDataPage)) {
        m_processDataPage->startScan();
    } else if (index == m_tabWidget->indexOf(m_shoppingListPage)) {
        m_shoppingListPage->startScan();
    }
}

void FleaMarketWidget::setTabVisible(Flohmarkt::Tab tab, bool visible)
{
    switch (tab) {
    case Flohmarkt::OrganizerOverviewTab:
        m_tabWidget->setTabVisible(m_tabWidget->indexOf(m_overviewPage), visible);
        break;
    case Flohmarkt::OrganizerProcessDataTab:
        m_tabWidget->setTabVisible(m_tabWidget->indexOf(m_processDataPage), visible);
        break;
    case Flohmarkt::OrganizerShoppingListTab:
        m_tabWidget->setTabVisible(m_tabWidget->indexOf(m_shoppingListPage), visible);
        break;
    case Flohmarkt::OrganizerItemListsTab:
        m_tabWidget->setTabVisible(m_tabWidget->indexOf(m_itemListsPage), visible);
        break;
    }

    int visibleTabs = 0;
    for (int i = 0; i < m_tabWidget->count(); i++) {
        visibleTabs += m_tabWidget->isTabVisible(i);
    }

    m_tabWidget->tabBar()->setVisible(visibleTabs > 1);
}
