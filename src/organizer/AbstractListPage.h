// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ABSTRACTLISTPAGE_H
#define ABSTRACTLISTPAGE_H

// Qt includes
#include <QWidget>
#include <QLocale>

class AbstractListPage : public QWidget
{
    Q_OBJECT

protected: // Functions
    explicit AbstractListPage(QWidget *parent = nullptr);
    QString getSaveFileName(const QString &name, bool consecutive = false);
    void setLastSavePath(const QString &fileName);
    const QLocale &locale() const;

private: // Variables
    QLocale m_locale;
    QString m_lastSavePath;

};

#endif // ABSTRACTLISTPAGE_H
