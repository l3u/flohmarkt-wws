// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef BUSYHELPER_H
#define BUSYHELPER_H

// Qt includes
#include <QObject>

// Qt classes
class QWidget;

class BusyHelper : public QObject
{
    Q_OBJECT

public:
    explicit BusyHelper(QWidget *parent, bool disable = false);
    ~BusyHelper();
    void reset();

private: // Variables
    QWidget *m_parent;
    const bool m_disable;
    bool m_resetCalled = false;

};

#endif // BUSYHELPER_H
