// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "ItemsListModel.h"

// Qt includes
#include <QDebug>

// C++ includes
#include <utility>

ItemsListModel::ItemsListModel(Database *database, QObject *parent)
    : QAbstractTableModel(parent),
      m_database(database)
{
}

int ItemsListModel::rowCount(const QModelIndex &) const
{
    return m_items.count();
}

int ItemsListModel::columnCount(const QModelIndex &) const
{
    return 4;
}

QVariant ItemsListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    switch (orientation) {
    case Qt::Horizontal:
        if (section == 0) {
            return tr("Beschreibung");
        } else if (section == 1) {
            return tr("Größe");
        } else if (section == 2) {
            return tr("Preis");
        } else if (section == 3) {
            return tr("Verkauft");
        }
        break;
    case Qt::Vertical:
        return m_items.at(section).listNumber;
    }

    // We can't reach here
    return QVariant();
}

QVariant ItemsListModel::data(const QModelIndex &index, int role) const
{
    if (! index.isValid()) {
        return QVariant();
    }

    const auto column = index.column();

    if (role == Qt::TextAlignmentRole) {
        if (column == 2) {
            return int(Qt::AlignRight | Qt::AlignVCenter);
        } else if (column == 3) {
            return Qt::AlignCenter;
        } else {
            return QVariant();
        }
    }

    const auto row = index.row();
    if (row >= m_items.count() || role != Qt::DisplayRole) {
        return QVariant();
    }

    if (column == 0) {
        return m_items.at(row).description;
    } else if (column == 1) {
        return m_items.at(row).size.isEmpty() ? tr("–") : m_items.at(row).size;
    } else if (column == 2) {
        if (role == Qt::DisplayRole) {
            return m_locale.toCurrencyString(m_items.at(row).price / 100.0);
        } else {
            return m_items.at(row).price;
        }
    } else if (column == 3) {
        return m_items.at(row).sold ? tr("\u2714") : QString();
    }

    // We can't reach here
    return QVariant();
}

Qt::ItemFlags ItemsListModel::flags(const QModelIndex &) const
{
    return Qt::ItemIsEnabled;
}

void ItemsListModel::setVendorId(const QString &vendorId)
{
    m_vendorId = vendorId;
    refresh();
}

void ItemsListModel::clear()
{
    const auto count = m_items.count();
    beginRemoveRows(QModelIndex(), 0, count - 1);
    m_soldItems = 0;
    m_salesVolume = 0;
    m_items.clear();
    endRemoveRows();
    const auto fistModelIndex = index(0, 0);
    const auto lastModelIndex = index(count, 0);
    Q_EMIT dataChanged(fistModelIndex, lastModelIndex, { Qt::DisplayRole });
}

void ItemsListModel::refresh()
{
    clear();

    const auto [ items, success ] = m_database->itemsForVendor(m_vendorId);
    if (! success) {
        return;
    }

    const auto lastRow = items.count() - 1;
    beginInsertRows(QModelIndex(), 0, lastRow);
    m_items = items;
    endInsertRows();

    m_soldItems = 0;
    m_salesVolume = 0;
    for (const auto &data : std::as_const(m_items)) {
        if (data.sold) {
            m_soldItems++;
            m_salesVolume += data.price;
        }
    }

    const auto firstModelIndex = index(0, 0);
    const auto lastModelIndex = index(lastRow, 0);
    Q_EMIT dataChanged(firstModelIndex, lastModelIndex, { Qt::DisplayRole });
}

int ItemsListModel::soldItems() const
{
    return m_soldItems;
}

int ItemsListModel::salesVolume() const
{
    return m_salesVolume;
}

const Database::ItemListData &ItemsListModel::itemData(int row) const
{
    return m_items[row];
}

const QVector<Database::ItemListData> &ItemsListModel::items() const
{
    return m_items;
}
