// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef FLEAMARKETWIDGET_H
#define FLEAMARKETWIDGET_H

// Local includes
#include "shared/Flohmarkt.h"

// Qt includes
#include <QWidget>

// Local classes
class Database;
class OverviewPage;
class ProcessDataPage;
class ShoppingListPage;
class ItemListsPage;

// Qt classes
class QTabWidget;

class FleaMarketWidget : public QWidget
{
    Q_OBJECT

public:
    explicit FleaMarketWidget(QWidget *parent = nullptr);
    bool createDatabase();
    bool openDatabase();
    void closeDatabase();
    void setTabVisible(Flohmarkt::Tab tab, bool visible);

Q_SIGNALS:
    void sqlError(const QString &text);

private Q_SLOTS:
    void processItem(const Flohmarkt::ItemData &data);
    void processVendor(const Flohmarkt::VendorData &data);
    void addToShoppingList(const Flohmarkt::ItemData &data);
    void tabChanged(int index);

private: // Variable
    Database *m_database;
    QTabWidget *m_tabWidget;
    OverviewPage *m_overviewPage;
    ProcessDataPage *m_processDataPage;
    ShoppingListPage *m_shoppingListPage;
    ItemListsPage *m_itemListsPage;

};

#endif // FLEAMARKETWIDGET_H
