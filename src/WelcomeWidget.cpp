// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "WelcomeWidget.h"

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

WelcomeWidget::WelcomeWidget(QWidget *parent) : QWidget(parent)
{
    auto *layout = new QVBoxLayout(this);

    layout->addStretch();

    auto *titleLabel = new QLabel(tr("Willkommen zum\nFlohmarkt-WWS!"));
    titleLabel->setAlignment(Qt::AlignHCenter);
    titleLabel->setStyleSheet(QStringLiteral("QLabel { font-weight: bold; font-size: %1pt }").arg(
        (int) double(titleLabel->font().pointSize()) * 2.0));
    layout->addWidget(titleLabel);

    auto *buttonLayout = new QGridLayout;
    layout->addLayout(buttonLayout);
    buttonLayout->addWidget(new QLabel, 0, 0, 4, 1);
    buttonLayout->addWidget(new QLabel, 0, 3, 4, 1);

    auto *welcomeLabel = new QLabel(tr("Bitte eine der folgenden Aktionen wählen:"));
    welcomeLabel->setAlignment(Qt::AlignHCenter);
    buttonLayout->addWidget(welcomeLabel, 0, 1, 1, 2);

    auto *vendorModeButton = new QPushButton(tr("Verkäuferunterlagen erstellen"));
    connect(vendorModeButton, &QPushButton::clicked,
            this, &WelcomeWidget::createVendorDocuments);
    buttonLayout->addWidget(vendorModeButton, 1, 1);

    auto *createFleaMarketDatabaseButton = new QPushButton(tr("Flohmarkt-Datenbank erstellen"));
    connect(createFleaMarketDatabaseButton, &QPushButton::clicked,
            this, &WelcomeWidget::createFleaMarketDatabase);
    buttonLayout->addWidget(createFleaMarketDatabaseButton, 1, 2);

    auto *openVendorDocumentsButton = new QPushButton(tr("Verkäuferunterlagen öffnen"));
    connect(openVendorDocumentsButton, &QPushButton::clicked,
            this, &WelcomeWidget::openVendorDocuments);
    buttonLayout->addWidget(openVendorDocumentsButton, 2, 1);

    auto *openDatabaseButton = new QPushButton(tr("Flohmarkt-Datenbank öffnen"));
    connect(openDatabaseButton, &QPushButton::clicked,
            this, &WelcomeWidget::openFleaMarketDatabase);
    buttonLayout->addWidget(openDatabaseButton, 2, 2);

    layout->addStretch();
}
