// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "TemporaryFile.h"

// Qt includes
#include <QDebug>
#include <QDir>

TemporaryFile::TemporaryFile(QObject *parent)
    : QTemporaryFile(QStringLiteral("flohmarkt_XXXXXX.html"), parent)
{
}

QString TemporaryFile::displayName() const
{
    return QDir::toNativeSeparators(fileName());
}

QString TemporaryFile::htmlUrl() const
{
    return url().toString().toHtmlEscaped();
}

QUrl TemporaryFile::url() const
{
    return QUrl::fromLocalFile(fileName());
}
