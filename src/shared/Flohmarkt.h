// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef FLOHMARKT_H
#define FLOHMARKT_H

#include <QString>
#include <QHash>

namespace Flohmarkt
{

// Shared enums

enum Mode {
    NoMode,
    VendorMode,
    OrganizerMode
};

enum Tab {
    OrganizerOverviewTab,
    OrganizerProcessDataTab,
    OrganizerShoppingListTab,
    OrganizerItemListsTab
};

// Shared structs

struct VendorData
{
    QString id;
    QString name;
    QString listNumber;
};

struct ItemData
{
    QString vendorId;
    QString itemId;
    int listNumber = -1;
    QString description;
    QString size;
    int price = 0;
};

// File format section

namespace Json
{

const QString metadata           = QStringLiteral("metadata");
const QString fileIdKey          = QStringLiteral("file id");
const QString fileId             = QStringLiteral("Flohmarkt-WWS vendor documents file");
const QString protocolVersionKey = QStringLiteral("protocol version");
constexpr int protocolVersion    = 1;

const QString vendorInfo       = QStringLiteral("vendor info");
const QString vendorName       = QStringLiteral("name");
const QString vendorListNumber = QStringLiteral("list number");
const QString vendorId         = QStringLiteral("id");
const QString vendorSurcharge  = QStringLiteral("surcharge");

const QString itemsList       = QStringLiteral("items list");
const QString itemId          = QStringLiteral("id");
const QString itemDescription = QStringLiteral("description");
const QString itemSize        = QStringLiteral("size");
const QString itemPrice       = QStringLiteral("price");

}

// QR code section

namespace Qr
{

// Header data

const QString codeId             = QStringLiteral("FWQC"); // Flohmarkt-WWS-QR-Code
constexpr int protocolVersion    = 1;

enum Type {
    VendorCode,
    ItemCode
};

const QHash<Type, QString> typeToString {
    { Type::VendorCode, QStringLiteral("v") },
    { Type::ItemCode,   QStringLiteral("i") }
};

const QHash<QString, Type> stringToType {
    { QStringLiteral("v"), Type::VendorCode },
    { QStringLiteral("i"), Type::ItemCode   }
};

// JSON keys

const QString vendorId         = QStringLiteral("v");
const QString vendorName       = QStringLiteral("n");
const QString vendorListNumber = QStringLiteral("l");

const QString itemId          = QStringLiteral("i");
const QString itemNumber      = QStringLiteral("n");
const QString itemDescription = QStringLiteral("d");
const QString itemSize        = QStringLiteral("s");
const QString itemPrice       = QStringLiteral("p");

}

}

#endif // FLOHMARKT_H
