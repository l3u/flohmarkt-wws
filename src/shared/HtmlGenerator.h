// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef HTMLGENERATOR_H
#define HTMLGENERATOR_H

// Qt includes
#include <QObject>
#include <QTextStream>

// Qt classes
class QIODevice;

class HtmlGenerator : public QObject
{
    Q_OBJECT

public:
    explicit HtmlGenerator(QIODevice *device, QObject *parent = nullptr);
    void setTitle(const QString &title);
    void setStyle(const QString &style);
    void startHtml();
    HtmlGenerator &operator<<(const char *data);
    HtmlGenerator &operator<<(const QString &data);
    HtmlGenerator &operator<<(int data);
    void finishHtml();

private: // Variables
    QTextStream m_stream;
    QString m_title;
    QString m_style;

};

#endif // HTMLGENERATOR_H
