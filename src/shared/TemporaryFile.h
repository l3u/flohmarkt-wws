// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TEMPORARYFILE_H
#define TEMPORARYFILE_H

// Qt includes
#include <QTemporaryFile>
#include <QUrl>

class TemporaryFile : public QTemporaryFile
{
    Q_OBJECT

public:
    explicit TemporaryFile(QObject *parent = nullptr);
    QString displayName() const;
    QString htmlUrl() const;
    QUrl url() const;

};

#endif // TEMPORARYFILE_H
