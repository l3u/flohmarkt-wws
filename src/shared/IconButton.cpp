// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "IconButton.h"

IconButton::IconButton(const QString &text, const QString &icon, bool flat, QWidget *parent)
    : QPushButton(text, parent)
{
    setIcon(QIcon(QStringLiteral(":/icons/%1.png").arg(icon)));
    setIconSize(QSize(24, 24));
    setFlat(flat);
}
