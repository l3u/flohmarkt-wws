// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef UUIDHELPER_H
#define UUIDHELPER_H

// Qt includes
#include <QString>

// Qt classes
class QJsonValue;

namespace UuidHelper
{

QString generateUuid();
bool checkUuid(const QJsonValue &value);

}

#endif // UUIDHELPER_H
