// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "QrCodeGenerator.h"
#include "Logging.h"

// qrencode includes
#include "qrcodegen/qrcodegen.hpp"

// Qt includes
#include <QJsonObject>
#include <QJsonDocument>
#include <QImage>
#include <QBuffer>
#include <QUrl>

namespace QrCodeGenerator
{

constexpr auto black = qRgb(  0,   0,   0);
constexpr auto white = qRgb(255, 255, 255);

QImage qrCode(const QByteArray &data)
{
    const auto qrCode = qrcodegen::QrCode::encodeText(data.constData(),
                                                      qrcodegen::QrCode::Ecc::MEDIUM);
    const auto size = qrCode.getSize();

    QImage image(size + 8, size + 8, QImage::Format_RGB32);
    image.fill(white);
    for (int y = 0; y < size; y++) {
        for (int x = 0; x < size; x++) {
            image.setPixel(x + 4, y + 4, qrCode.getModule(x, y) ? black : white);
        }
    }

    qCDebug(FlohmarktLog) << "Created QR Code for data:\n" <<  QString::fromUtf8(data);
    return image;
}

QImage qrCode(QJsonObject data, Flohmarkt::Qr::Type type)
{
    QByteArray rawData;

    // Code ID header (4 chars)
    rawData.append(Flohmarkt::Qr::codeId.toUtf8());

    // Protocol version (2 chars that represent the hex numer as ASCII characters)
    rawData.append(QString::number(Flohmarkt::Qr::protocolVersion, 16).rightJustified(
                                    2, QChar::fromLatin1('0')).toUtf8());

    // Code type (1 char, either "v" for "vendor code" or "i" for "item code")
    rawData.append(Flohmarkt::Qr::typeToString.value(type).toUtf8());

    // The JSON representation of the actual data
    rawData.append(QJsonDocument(data).toJson(QJsonDocument::Compact));

    return qrCode(rawData);
}

QString base64Source(const QImage &image)
{
    QByteArray byteArray;
    QBuffer buffer(&byteArray);
    buffer.open(QIODevice::WriteOnly);
    image.save(&buffer, "PNG");
    buffer.close();
    return QStringLiteral("data:image/png;base64,%1").arg(QLatin1String(byteArray.toBase64()));
}

QString escape(const QString &string)
{
    static const auto s_exclude = QStringLiteral(" !\"#$&'()*+,/:;=?@[]").toUtf8();
    const auto byteArray = string.toUtf8();
    return QString::fromUtf8(byteArray.toPercentEncoding(s_exclude));
}

QString unEscape(const QString &string)
{
    return QUrl::fromPercentEncoding(string.toUtf8());
}

}
