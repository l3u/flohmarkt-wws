// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ICONBUTTON_H
#define ICONBUTTON_H

// Qt includes
#include <QPushButton>

class IconButton : public QPushButton
{
    Q_OBJECT

public:
    explicit IconButton(const QString &text, const QString &icon, bool flat = false,
                        QWidget *parent = nullptr);

};

#endif // ICONBUTTON_H
