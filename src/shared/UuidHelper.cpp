// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "UuidHelper.h"

// Qt includes
#include <QDebug>
#include <QUuid>
#include <QJsonValue>

namespace UuidHelper
{

QString generateUuid()
{
    const auto id = QUuid::createUuid();
    return id.toString(QUuid::WithoutBraces);
}

bool checkUuid(const QJsonValue &value)
{
    const auto uuidString = value.toString();
    if (uuidString.count() != 36) {
        return false;
    }

    const auto uuid = QUuid::fromString(uuidString);
    if (uuid.isNull()) {
        return false;
    }

    return true;
}

}
