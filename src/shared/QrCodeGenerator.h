// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef QRCODEGENERATOR_H
#define QRCODEGENERATOR_H

// Local includes
#include "shared/Flohmarkt.h"

// Qt includes
#include <QImage>
#include <QString>
#include <QByteArray>

namespace QrCodeGenerator
{

QImage qrCode(const QByteArray &data);
QImage qrCode(QJsonObject data, Flohmarkt::Qr::Type type);
QString base64Source(const QImage &image);
QString escape(const QString &string);
QString unEscape(const QString &string);

}

#endif // QRCODEGENERATOR_H
