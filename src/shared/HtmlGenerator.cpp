// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "HtmlGenerator.h"

// Qt includes
#include <QDebug>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QTextCodec>
#endif

HtmlGenerator::HtmlGenerator(QIODevice *device, QObject *parent) : QObject(parent)
{
    m_stream.setDevice(device);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    m_stream.setCodec(QTextCodec::codecForName("UTF-8"));
#endif
}

void HtmlGenerator::setTitle(const QString &title)
{
    m_title = title;
}

void HtmlGenerator::setStyle(const QString &style)
{
    m_style = style;
}

void HtmlGenerator::startHtml()
{
    m_stream << "<!doctype html>\n\n"
             << "<html lang=\"de\">\n\n"
             << "<head>\n\n"
             << "<title>"
             << m_title
             << "</title>\n\n"
             << "<meta charset=\"utf-8\">\n"
             << "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n\n"
             << "<style>\n"
             << m_style
             << "</style>\n\n"
             << "</head>\n\n"
             << "<body>\n\n";
}

HtmlGenerator &HtmlGenerator::operator<<(const char *data)
{
    m_stream << QString::fromUtf8(data);
    return *this;
}

HtmlGenerator &HtmlGenerator::operator<<(const QString &data)
{
    m_stream << data;
    return *this;
}

HtmlGenerator &HtmlGenerator::operator<<(int data)
{
    m_stream << QString::number(data);
    return *this;
}

void HtmlGenerator::finishHtml()
{
    m_stream << "</body>\n\n"
             << "</html>\n";
}
