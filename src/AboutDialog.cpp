// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "AboutDialog.h"
#include "version.h"

// Qt includes
#include <QVBoxLayout>
#include <QLabel>
#include <QDate>
#include <QLocale>
#include <QDialogButtonBox>

AboutDialog::AboutDialog(QWidget *parent) : QDialog(parent)
{
    auto *layout = new QVBoxLayout(this);
    QLabel *currentLabel;

    currentLabel = new QLabel(tr("Flohmarkt-WWS"));
    currentLabel->setAlignment(Qt::AlignHCenter);
    currentLabel->setStyleSheet(QStringLiteral("QLabel { font-weight: bold; font-size: %1pt }").arg(
        (int) double(currentLabel->font().pointSize()) * 2.0));
    layout->addWidget(currentLabel);

    currentLabel = new QLabel(tr("<b>das Flohmarkt-Warenwirtschafts-System</b>"));
    currentLabel->setAlignment(Qt::AlignHCenter);
    layout->addWidget(currentLabel);

    currentLabel = new QLabel(tr("Version %1").arg(QLatin1String(FWWS_VERSION)));
    currentLabel->setAlignment(Qt::AlignHCenter);
    layout->addWidget(currentLabel);

    const auto compilerYear = QLocale(QStringLiteral("en_US")).toDate(
        QStringLiteral(__DATE__).simplified(), QStringLiteral("MMM d yyyy")).year();
    const auto toYear = compilerYear == 2023 ? QString()
                                             : QStringLiteral("-%1").arg(compilerYear);

    currentLabel = new QLabel(
        tr("<hr>"
           "<p>Quellcode veröffentlicht unter der<br/>"
           "<a href=\"https://www.gnu.org/licenses/#GPL\">GNU General Public License (GPL)</a></p>"
           "<p>Kompiliert gegen <a href=\"https://www.qt.io/\">Qt</a> %1</p>").arg(
           QLatin1String(QT_VERSION_STR)));

    currentLabel->setAlignment(Qt::AlignCenter);
    currentLabel->setTextInteractionFlags(Qt::TextBrowserInteraction | Qt::TextSelectableByMouse);
    currentLabel->setOpenExternalLinks(true);
    layout->addWidget(currentLabel);

    currentLabel = new QLabel(
        tr("<hr>"
           "<p>Copyright \u00A9 2023%1 Tobias Leupold<br>"
           "<a href=\"https://gitlab.com/l3u/flohmarkt-wws/\">"
           "https://gitlab.com/l3u/flohmarkt-wws/"
           "</a></p>").arg(toYear));
    currentLabel->setAlignment(Qt::AlignCenter);
    currentLabel->setTextInteractionFlags(Qt::TextBrowserInteraction | Qt::TextSelectableByMouse);
    currentLabel->setOpenExternalLinks(true);
    layout->addWidget(currentLabel);

    currentLabel = new QLabel(
        tr("<hr>"
           "<p>Die QR-Codes werden mit Hilfe der "
           "<a href=\"https://www.nayuki.io/page/qr-code-generator-library\">"
           "QR&nbsp;Code&nbsp;generator&nbsp;library</a><br>"
           "vom Project Nayuki erzeugt, veröffentlicht unter der "
           "<a href=\"https://opensource.org/license/mit/\">MIT-Lizenz</a>.</p>"));
    currentLabel->setAlignment(Qt::AlignCenter);
    currentLabel->setTextInteractionFlags(Qt::TextBrowserInteraction | Qt::TextSelectableByMouse);
    currentLabel->setOpenExternalLinks(true);
    layout->addWidget(currentLabel);

    currentLabel = new QLabel(
        tr("<hr>"
           "<p>Die Icons kommen vom <a href=\"https://invent.kde.org/frameworks/breeze-icons\">"
           "Breeze-Paket</a> des <a href=\"https://kde.org/\">KDE-Projekts</a>.</p>"));
    currentLabel->setAlignment(Qt::AlignCenter);
    currentLabel->setTextInteractionFlags(Qt::TextBrowserInteraction | Qt::TextSelectableByMouse);
    currentLabel->setOpenExternalLinks(true);
    layout->addWidget(currentLabel);

    currentLabel = new QLabel(tr("<hr>"));
    layout->addWidget(currentLabel);

    auto *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    layout->addWidget(buttonBox);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
}
