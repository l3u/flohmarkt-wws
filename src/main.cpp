// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Local includes
#include "MainWindow.h"
#include "shared/Logging.h"

// Qt includes
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);

    // Set application-wide style sheets

    const QString style = QStringLiteral(
        "QGroupBox { "
            "font-weight: bold; "
        "} "
    );

    application.setStyleSheet(style);

    // Install translators

    const QLocale locale;
    const auto qmSeparator = QStringLiteral("_");
    const auto qmSuffix = QStringLiteral(".qm");

    // Look for a Qt translation
    QTranslator qtTranslator;
    const auto qtbaseQm = QStringLiteral("qtbase");

    // Look for a system-wide installed Qt translation file
    if (! qtTranslator.load(locale, qtbaseQm, qmSeparator,
                            QLibraryInfo::location(QLibraryInfo::TranslationsPath), qmSuffix)) {

        qCDebug(FlohmarktLog) << "Could not load a system-wide installed Qt translation,"
                              << "checking resources";

        // Look for a resource shipped translation
        if (! qtTranslator.load(locale, qtbaseQm, qmSeparator, QStringLiteral(":"), qmSuffix)) {
            qCWarning(FlohmarktLog) << "Could not initialize a translator for Qt!";
        }
    }

    // Install the translator if we found translation data
    if (! qtTranslator.isEmpty()) {
        qCDebug(FlohmarktLog) << "Loaded Qt translation:" << qtTranslator.filePath();
        application.installTranslator(&qtTranslator);
    }

    // Construct the main window
    MainWindow mainWindow;
    mainWindow.show();

    return application.exec();
}
