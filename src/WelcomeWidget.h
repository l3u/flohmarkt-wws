// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef WELCOMEWIDGET_H
#define WELCOMEWIDGET_H

// Local includes
#include "shared/Flohmarkt.h"

// Qt includes
#include <QWidget>

class WelcomeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit WelcomeWidget(QWidget *parent = nullptr);

Q_SIGNALS:
    void createVendorDocuments();
    void openVendorDocuments();
    void createFleaMarketDatabase();
    void openFleaMarketDatabase();

};

#endif // WELCOMEWIDGET_H
