# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: BSD-3-Clause

CONFIG += c++17
CONFIG += qt
CONFIG += static

QT += widgets
QT += sql

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050F00

src=$$PWD/../src
INCLUDEPATH += $$src

RESOURCES = $$PWD/resources.qrc

TARGET = flohmarkt-wws
