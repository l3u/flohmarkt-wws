@rem SPDX-FileCopyrightText: none
@rem
@rem SPDX-License-Identifier: CC0-1.0

set PATH=C:\Qt\mingw810_32\bin;%PATH%
set QT=C:\Qt\5.15.8_static

%QT%\bin\lconvert -o ..\qtbase_de.qm %QT%\translations\qtbase_de.qm
%QT%\bin\qmake flohmarkt-wws.pro

mingw32-make -j3 release

copy release\flohmarkt-wws.exe .\..\..\..\flohmarkt-wws-@VERSION@_unpacked.exe
copy release\flohmarkt-wws.exe .\..\..\..\flohmarkt-wws-@VERSION@.exe

upx --best .\..\..\..\flohmarkt-wws-@VERSION@.exe
