# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: BSD-3-Clause

include(../shared_config.pri)
include(sources.pri)

QMAKE_TARGET_COMPANY = nasauber.de
QMAKE_TARGET_DESCRIPTION = "Das Flohmarkt-Warenwirtschafts-System"
QMAKE_TARGET_COPYRIGHT = "Copyright (c) @COPYRIGHT@ Tobias Leupold"
QMAKE_TARGET_PRODUCT = Flohmarkt-WWS
VERSION = @VERSION@
