#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

version=$(git describe --dirty)
version=${version##*v}

mkdir windows_files

tar -xJf flohmarkt-wws-$version.tar.xz -C windows_files/

release_base=windows_files/flohmarkt-wws-$version

# Generate debugMode.h
sed "s/#cmakedefine DEBUG_MODE/\/\* #undef DEBUG_MODE \*\//" \
    $release_base/src/debugMode.h.in > $release_base/src/debugMode.h
rm $release_base/src/debugMode.h.in

# Update the qmake project file version
sed -i "s/@VERSION@/$version/" $release_base/res/windows_build/flohmarkt-wws.pro

# Update the qmake project file copyright
copyright="2023"
currentYear=$(date +%Y)
if [[ "$currentYear" != "2023" ]]; then
    copyright="2023-$(date +%Y)"
fi
sed -i "s/@COPYRIGHT@/$copyright/" $release_base/res/windows_build/flohmarkt-wws.pro

# Update the build batch file version
sed -i "s/@VERSION@/$version/" $release_base/res/windows_build/make_windows_build.bat

# Add the qm file built by the build script to resources.qrc
sed -i "s/<!-- qtbase_de.qm  -->/<file>qtbase_de.qm<\/file>/" $release_base/res/resources.qrc

# Generate the qmake sources list
./create_sources_list.sh $release_base > $release_base/res/windows_build/sources.pri
