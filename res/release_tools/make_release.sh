#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

# Fill in variables
git_root=$(git rev-parse --show-toplevel)
version=$(git describe --dirty)
version=${version##*v}

# Create snapshot
mkdir flohmarkt-wws-$version
cd "$git_root"
git archive HEAD | tar -xC res/release_tools/flohmarkt-wws-$version

# >>> Get in the released sources
cd res/release_tools/flohmarkt-wws-$version

# Generate version.h
sed "s/@VERSION@/$version/" src/version.h.in > src/version.h
rm src/version.h.in

# Remove the .gitignore file
rm .gitignore

# <<< Get out of the released sources
cd ..

# Create the release tarball
tar -cJf flohmarkt-wws-$version.tar.xz flohmarkt-wws-$version
rm -r flohmarkt-wws-$version
