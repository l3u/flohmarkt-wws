# Flohmarkt-WWS

Das Flohmarkt-Warenwirtschafts-System. Soll es zumindest werden ;-) Was es tut, kommt demnächst …

Das Programm ist in C++/[Qt](https://www.qt.io/) implementiert (es kommt der C++17-Standard zum Einsatz) und wird unter den Bedingungen der [GNU General Public License (GPL)](https://www.gnu.org/licenses/#GPL) veröffentlicht.

Zum Erstellen der QR-Codes kommt die [QR Code generator library](https://www.nayuki.io/page/qr-code-generator-library) vom Project Nayuki zum Einsatz, veröffentlicht unter der [MIT-Lizenz](https://opensource.org/license/mit/).

Die Icons kommen vom [Breeze-Paket](https://invent.kde.org/frameworks/breeze-icons) des [KDE-Projekts](https://kde.org/).
